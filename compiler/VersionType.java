
public class VersionType extends BaseType {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("Version");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("Version");
	}

	@Override
	public void typeCheck() {}
	
	@Override
	public Expression getDefaultValue() {
		return new Expression() {

			@Override
			public StringBuilder stringify() {
				return new StringBuilder("0");
			}

			@Override
			public StringBuilder transpile() {
				return new StringBuilder("0");
			}

			@Override
			public void typeCheck() {}

			@Override
			public BaseType getTypeImpl() {
				return new VersionType();
			}
			
		};
	}
	
	@Override
	public boolean isEqualityComparableTo(BaseType other) {
		return other.isEqualityComparableTo(this);
	}
	
	@Override
	public boolean isEqualityComparableTo(VersionType other) {
		return true;
	}
	
}
