
public class IntegerGetTypeStrategy extends BinaryOperatorGetTypeStrategy {

	@Override
	public BaseType binaryGetType(BaseType t1, BaseType t2) {
		return t1.bitWidth() > t2.bitWidth() ? t1 : t2;
	}

}
