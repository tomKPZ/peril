
public class ArrayType extends BaseType {
	
	private QualifiedType qt;
	
	ArrayType(QualifiedType qt) {
		this.qt = qt;
	}
	
	public QualifiedType getElementType() {
		return qt;
	}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("[").append(qt.stringify()).append("]");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("SparseArray<").append(qt.transpile()).append("> *");
	}

	@Override
	public void typeCheck() {
		qt.typeCheck();
	}

	@Override
	public Expression getDefaultValue() {
		return new NullExpression();
	}
	
	@Override
	public boolean isIndexable() {
		return true;
	}
	
	@Override
	public QualifiedType getIndexedType() {
		return qt;
	}
	
	@Override
	public StringBuilder transpileNewExpression() {
		return new StringBuilder("new SparseArray<").append(getElementType().transpile()).append(">()");
	}
	
	@Override
	public boolean isEqualityComparableTo(NullType other) {
		return true;
	}
	
	@Override
	public boolean isEqualityComparableTo(BaseType other) {
		return other.isEqualityComparableTo(this);
	}

	@Override
	public boolean isEqualityComparableTo(ArrayType other) {
		return qt.qualifierList.equals(other.qt.qualifierList) &&
				qt.baseType.isEqualityComparableTo(other.qt.baseType);
	}

}
