
public class BinaryOperatorBitwiseXor extends BinaryOperator {

	public BinaryOperatorBitwiseXor(Expression e1, Expression e2) {
		super(e1, "^", e2);
	}

	@Override
	protected String getTranspiledOpString() {
		return "^";
	}

	@Override
	protected BinaryOperatorTypeCheckStrategy getTypeCheckStrategy() {
		return new NumericTypeCheckStrategy();
	}

	@Override
	protected BinaryOperatorGetTypeStrategy getGetTypeStrategy() {
		return new IntegerGetTypeStrategy();
	}

}
