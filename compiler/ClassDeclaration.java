import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ClassDeclaration extends Statement {
	
	private String className;
	private List<ClassMemberDeclaration> variableDeclarations;
	private List<String> fieldNames;
	private Map<String, QualifiedType> fields;
	
	public ClassDeclaration(String className) {
		this.className = className;
		variableDeclarations = new ArrayList<ClassMemberDeclaration>();
		fieldNames = new ArrayList<String>();
		fields = new HashMap<String, QualifiedType>();
	}
	
	public String getClassName() {
		return className;
	}
	
	public QualifiedType getFieldType(String fieldName) {
		if(!fields.containsKey(fieldName)) {
			throw new RuntimeException("Class " + className + " has no member " + fieldName);
		}
		return fields.get(fieldName);
	}
	
	protected void setTypeForField(String name, QualifiedType type) {
		fields.put(name, type);
	}
	
	protected void addField(String name) {
		if(fields.containsKey(name)) {
			throw new RuntimeException("Duplicate field " + name + " in class " + className);
		}
		fieldNames.add(name);
	}
	
	public void addField(ClassMemberDeclaration variableDeclaration) {
		for(int i = 0; i < variableDeclaration.nVariables(); i++) {
			addField(variableDeclaration.getVariableName(i));
		}
		variableDeclarations.add(variableDeclaration);
	}

	@Override
	public StringBuilder stringify() {
		StringBuilder sb = new StringBuilder("class ").append(className).append(" {\n");
		for(String fieldName : fieldNames) {
			sb.append(fields.get(fieldName).stringify()).append(" ").append(fieldName).append(";\n");
		}
		return sb.append("} ");
	}
	
	@Override
	public int nTranspiledStatements() {
		return 2;
	}

	@Override
	public StringBuilder transpile() {
		StringBuilder sb = getDeclaration();
		return sb.append("\n").append(getInitializer());
	}
	
	public StringBuilder getDeclaration() {
		StringBuilder sb = new StringBuilder("struct ").append(NameMangler.mangle(className)).append(" {\n");
		for(String fieldName : fieldNames) {
			sb.append(fields.get(fieldName).transpile()).append(" *").append(NameMangler.mangle(fieldName)).append(";\n");
		}
		return sb.append("};");
	}
	
	public StringBuilder getInitializer() {
		StringBuilder sb = new StringBuilder();
		sb.append("const auto _new_").append(className).append(" = [=] () {\n");
		sb.append(NameMangler.mangle(className)).append(" *_this = new ").append(NameMangler.mangle(className)).append("();\n");
		for(ClassMemberDeclaration variableDeclaration : variableDeclarations) {
			sb.append(variableDeclaration.transpileVariableDeclaration(className)).append("\n");
		}
		sb.append("return _this;\n");
		return sb.append("};");
	}
	
	protected boolean variableTypesAdded = false;
	
	public void addVariableTypes() {
		if(!variableTypesAdded) {
			for(ClassMemberDeclaration variableDeclaration : variableDeclarations) {
				for(int i = 0; i < variableDeclaration.nVariables(); i++) {
					setTypeForField(variableDeclaration.getVariableName(i), variableDeclaration.getVariableType(i));
				}
			}
			variableTypesAdded = true;
		}
	}
	
	public void typeCheckDefinition() {
		Scope.getInstance().enterClassDeclaration(this);
		
		QualifiedType thisType = new QualifiedType(new QualifierList().addQualifier("transient"), new ObjectType(className));
		Scope.getInstance().addVariable("this", thisType);
		thisType.typeCheck();
		
		addVariableTypes();
		for(ClassMemberDeclaration variableDeclaration : variableDeclarations) {
			variableDeclaration.typeCheck();
		}
		
		// make sure each variable type exists
		for(Entry<String, QualifiedType> field : fields.entrySet()) {
			field.getValue().typeCheck();
		}
		// TODO: verify that member initializers are compatible with their member type
		Scope.getInstance().leaveScope();
	}

	@Override
	public void typeCheck() {
		Scope.getInstance().addUserType(this);
		typeCheckDefinition();
	}
	
	@Override
	public boolean isClassDeclaration() {
		return true;
	}
	
	@Override
	public StringBuilder getForwardDeclaration() {
		return new StringBuilder("struct ").append(NameMangler.mangle(className)).append(";");
	}
	
}
