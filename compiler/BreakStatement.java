
public class BreakStatement extends Statement {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("break;");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("break;");
	}

	@Override
	public void typeCheck() {
		if(!Scope.getInstance().isInLoop()) {
			throw new RuntimeException("Must be in a loop to break");
		}
	}

}
