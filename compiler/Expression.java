
public abstract class Expression implements AstNode {
	
	BaseType type = null;
	
	public final BaseType getType() {
		if(type == null) {
			type = getTypeImpl();
		}
		return type;
	}
	
	public abstract BaseType getTypeImpl();
	
	public int nExpressions() {
		return 1;
	}
	
	public Expression getSubexpression(int index) {
		int nSubtypes = this.getType().nSubtypes();
		if(nSubtypes == 0 || nSubtypes > 1) {
			if(index < 0 || index >= nExpressions()) {
				throw new RuntimeException("Index " + index + " into expression " + stringify() + " was out of bounds");
			}
			return new TupleGetElement(this, index);
		}
		if(index != 0) {
			throw new RuntimeException("Index " + index + " into expression " + stringify() + " was out of bounds for non-tuple expression");
		}
		return this;
	}
	
	public boolean isExpressionAssignable() {
		return false;
	}
	
}
