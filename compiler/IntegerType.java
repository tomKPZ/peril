
public abstract class IntegerType extends BaseType {

	@Override
	public boolean isIntegralValue() {
		return true;
	}
	
	@Override
	public boolean isNumeric() {
		return true;
	}
	
	@Override
	public boolean isAddableTo(BaseType other) {
		return other.isAddableTo(this);
	}
	
	@Override
	public boolean isAddableTo(IntegerType other) {
		return true;
	}
	
	@Override
	public BaseType getAddedType(BaseType other) {
		return other.getAddedType(this);
	}
	
	@Override
	public BaseType getAddedType(IntegerType other) {
		return bitWidth() > other.bitWidth() ? this : other;
	}
	
	@Override
	public boolean isEqualityComparableTo(BaseType other) {
		return other.isEqualityComparableTo(this);
	}
	
	@Override
	public boolean isEqualityComparableTo(IntegerType other) {
		return true;
	}
	
	@Override
	public Expression getDefaultValue() {
		return new IntegerConstant("0");
	}
	
	@Override
	public void typeCheck() {
		
	}
	
	@Override
	public StringBuilder transpile() {
		return new StringBuilder("int" + bitWidth() + "_t");
	}
	
}
