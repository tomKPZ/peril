
public class Function extends Expression {
	private StatementList body;
	private NamedTuple arguments;
	private BaseType returnType;
	
	public Function(NamedTuple arguments, BaseType returnType, StatementList statementList) {
		this.arguments = arguments;
		this.returnType = returnType;
		this.body = statementList;
		if(new TupleType().isAssignableFrom(returnType)) {
			this.body.add(new ReturnStatement(new TupleExpression()));
		}
	}
	
	public StringBuilder stringify() {
		StringBuilder sb = new StringBuilder();
		sb.append(arguments.stringify());
		sb.append(" -> ");
		sb.append(returnType.stringify());
		sb.append(" {\n");
		sb.append(body.stringify());
		return sb.append("}");
	}
	
	public NamedTuple getArguments() {
		return arguments;
	}

	@Override
	public StringBuilder transpile() {
		StringBuilder sb = new StringBuilder(getType().transpile());
		sb.append("([=] ");
		sb.append(arguments.transpile());
		sb.append(" mutable -> ");
		sb.append(returnType.transpile());
		sb.append(" {\n");
		
		// make arguments optionally persistent
		for(NamedTuple.Field field : arguments.fields) {
			sb.append(field.getType().transpile()).append(" *").append(field.getName()).append(" = new ").append(field.getType().transpile()).append("(_").append(field.getName()).append(");\n");
		}
		
		sb.append(body.transpile());
		return sb.append("})");
	}

	@Override
	public BaseType getTypeImpl() {
		return new FunctionType(arguments.toBaseType(), returnType);
	}

	@Override
	public void typeCheck() {
		returnType.typeCheck();
		arguments.typeCheck();
		
		Scope.getInstance().enterFunction(returnType);
		
		for(NamedTuple.Field field : arguments.fields) {
			Scope.getInstance().addVariable(field.getName(), field.getType());
		}
		
		body.typeCheck();
		
		Scope.getInstance().leaveScope();
	}
}
