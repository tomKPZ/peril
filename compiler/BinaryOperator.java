
public abstract class BinaryOperator extends Expression {
	
	protected Expression e1, e2;
	protected String op;
	
	public BinaryOperator(Expression e1, String op, Expression e2) {
		this.e1 = e1;
		this.op = op;
		this.e2 = e2;
	}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("(").append(e1.stringify()).append(") ").append(op).append(" (").append(e2.stringify()).append(")");
	}
	
	protected abstract String getTranspiledOpString();

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("(").append(e1.transpile()).append(") ").append(getTranspiledOpString()).append(" (").append(e2.transpile()).append(")");
	}
	
	protected abstract BinaryOperatorTypeCheckStrategy getTypeCheckStrategy();

	@Override
	public void typeCheck() {
		e1.typeCheck();
		e2.typeCheck();
		getTypeCheckStrategy().binaryTypeCheck(e1.getType(), e2.getType());
	}
	
	protected abstract BinaryOperatorGetTypeStrategy getGetTypeStrategy();

	@Override
	public BaseType getTypeImpl() {
		return getGetTypeStrategy().binaryGetType(e1.getType(), e2.getType());
	}
}
