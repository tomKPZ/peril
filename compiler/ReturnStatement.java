
public class ReturnStatement extends Statement {
	
	private Expression exp;
	public ReturnStatement(Expression exp) {
		this.exp = exp;
	}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("return ").append(exp.stringify()).append(";");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("return ").append(exp.transpile()).append(";");
	}

	@Override
	public void typeCheck() {
		exp.typeCheck();
		if(!Scope.getInstance().isInFunction()) {
			throw new RuntimeException("Can only return from within a funcion body");
		}
		if(!Scope.getInstance().getEnclosingFunctionReturnType().isAssignableFrom(exp.getType())) {
			throw new RuntimeException("Returned expression type " + exp.getType().stringify() + " is incompatible with function return type " + Scope.getInstance().getEnclosingFunctionReturnType().stringify());
		}
	}

}
