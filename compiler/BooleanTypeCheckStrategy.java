
public class BooleanTypeCheckStrategy extends BinaryOperatorTypeCheckStrategy {

	@Override
	public void binaryTypeCheck(BaseType t1, BaseType t2) {
		if(!t1.isConditional() || !t2.isConditional()) {
			throw new RuntimeException("Cannot use binary boolean operator on types " + t1.stringify() + " and" + t2.stringify());
		}
	}

}
