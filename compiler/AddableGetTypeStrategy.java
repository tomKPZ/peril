
public class AddableGetTypeStrategy extends BinaryOperatorGetTypeStrategy {

	@Override
	public BaseType binaryGetType(BaseType t1, BaseType t2) {
		return t1.getAddedType(t2);
	}

}
