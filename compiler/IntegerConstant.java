import java.math.BigInteger;

public class IntegerConstant extends Expression {
	
	protected String value;
	protected BaseType type; 
	
	public IntegerConstant(String value) {
		this.value = value;
		BigInteger b = new BigInteger(value);
		if(b.compareTo(BigInteger.valueOf(Byte.MIN_VALUE)) >= 0 &&
				b.compareTo(BigInteger.valueOf(Byte.MAX_VALUE)) <= 0) {
			type = new IntType(); //ByteType();
		} else if(b.compareTo(BigInteger.valueOf(Short.MIN_VALUE)) >= 0 &&
				b.compareTo(BigInteger.valueOf(Short.MAX_VALUE)) <= 0) {
			type = new IntType(); //ShortType();
		} else if(b.compareTo(BigInteger.valueOf(Integer.MIN_VALUE)) >= 0 &&
				b.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) <= 0) {
			type = new IntType();
		} else if(b.compareTo(BigInteger.valueOf(Long.MIN_VALUE)) >= 0 &&
				b.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) <= 0) {
			type = new LongType();
		} else {
			throw new RuntimeException("Integer constant " + value + " is too big to fit into 64 bits");
		}
	}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder(value);
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder(value);
	}

	@Override
	public BaseType getTypeImpl() {
		return type;
	}

	@Override
	public void typeCheck() {}
}
