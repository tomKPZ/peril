
public class WhileLoop extends Statement {
	
	private Expression condition;
	private Statement s;
	
	public WhileLoop(Expression exp, Statement s) {
		this.condition = exp;
		this.s = s;
	}
	@Override
	public StringBuilder stringify() {
		return new StringBuilder("while (").append(condition.stringify()).append(") ").append(s.stringify()).append(" ");
	}
	@Override
	public StringBuilder transpile() {
		return new StringBuilder("while (").append(condition.transpile()).append(") {\n").append(s.transpile()).append(" ").append("\n}");
	}
	@Override
	public void typeCheck() {
		condition.typeCheck();
		if(!condition.getType().isConditional()) {
			throw new RuntimeException("Condition " + condition.stringify() + " in for loop was of type " + condition.getType().stringify() + ", not bool");
		}
		Scope.getInstance().enterLoop();
		s.typeCheck();
		Scope.getInstance().leaveScope();
	}
}
