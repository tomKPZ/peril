
public class BinaryOperatorLogicalOr extends BinaryOperator {

	public BinaryOperatorLogicalOr(Expression e1, Expression e2) {
		super(e1, "or", e2);
	}

	@Override
	protected String getTranspiledOpString() {
		return "||";
	}

	@Override
	protected BinaryOperatorTypeCheckStrategy getTypeCheckStrategy() {
		return new BooleanTypeCheckStrategy();
	}

	@Override
	protected BinaryOperatorGetTypeStrategy getGetTypeStrategy() {
		return new BooleanGetTypeStrategy();
	}

}
