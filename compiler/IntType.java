
public class IntType extends IntegerType {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("int");
	}

	@Override
	public int bitWidth() {
		return 32;
	}
	
}
