
public class PrintStatement extends Statement {
	private Expression exp;
	private boolean newLine;
	public PrintStatement(Expression exp, boolean newLine) {
		this.exp = exp;
		this.newLine = newLine;
	}
	@Override
	public StringBuilder stringify() {
		StringBuilder sb = new StringBuilder();
		if(newLine) {
			sb.append("println");
		} else {
			sb.append("print");
		}
		return sb.append(" ").append(exp.stringify()).append(";");
	}
	@Override
	public StringBuilder transpile() {
		StringBuilder sb = new StringBuilder("std::cout << (");
		BaseType expType = exp.getType();
		boolean needsCast = expType.isIntegralValue() && expType.bitWidth() == 8;
		if(needsCast) {
			sb.append("static_cast<int16_t>(");
		}
		sb.append(exp.transpile()).append(")");
		if(needsCast) {
			sb.append(")");
		}
		if(newLine) {
			sb.append(" << std::endl");
		}
		return sb.append(";");
	}
	@Override
	public void typeCheck() {
		exp.typeCheck();
	}
}
