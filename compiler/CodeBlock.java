
public class CodeBlock extends Statement {
	
	private StatementList body;
	
	public CodeBlock(StatementList body) {
		this.body = body;
	}
	
	@Override
	public StringBuilder stringify() {
		return new StringBuilder("{\n").append(body.stringify()).append("}");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("{\n").append(body.transpile()).append("}");
	}

	@Override
	public void typeCheck() {
		Scope.getInstance().enterScope();
		body.typeCheck();
		Scope.getInstance().leaveScope();
	}
}
