
public abstract class BaseType {
	public abstract StringBuilder stringify();
	public abstract StringBuilder transpile();
	public abstract void typeCheck();
	public abstract Expression getDefaultValue();
	
	public int nSubtypes() {
		return 1;
	}
	
	public BaseType getSubtype(int index) {
		if(index != 0) {
			throw new RuntimeException("Cannot get element " + index + " of non-tuple type " + stringify());
		} else {
			return this;
		}
	}
	
	public ClassDeclaration getClassDeclaration() {
		throw new RuntimeException("Type " + stringify() + " is not a class type");
	}
	
	public BaseType getMemberType(String member) {
		throw new RuntimeException(stringify() + " has no member " + member);
	}
	
	public int bitWidth() {
		throw new RuntimeException("Type " + stringify() + " does not have a bit width");
	}
	
	public boolean isConditional() {
		return false;
	}
	
	public boolean isIntegralValue() {
		return false;
	}
	
	public boolean isNumeric() {
		return false;
	}
	
	public boolean isIndexable() {
		return false;
	}
	
	public QualifiedType getIndexedType() {
		throw new RuntimeException("Cannot index into " + stringify());
	}
	
	public boolean isCallable() {
		return false;
	}
	
	public BaseType getArgumentsType() {
		throw new RuntimeException("Type " + stringify() + " does not take arguments");
	}
	
	public BaseType getReturnType() {
		throw new RuntimeException("Type " + stringify() + " does not return anything");
	}
	
	public StringBuilder transpileNewExpression() {
		throw new RuntimeException("Cannot instantiate type " + stringify());
	}
	
	public boolean isAddableTo(BaseType other) {
		return false;
	}
	
	public boolean isAddableTo(StringType other) {
		return false;
	}
	
	public boolean isAddableTo(IntegerType other) {
		return false;
	}
	
	public BaseType getAddedType(BaseType other) {
		throw new RuntimeException("Cannot add " + stringify() + " to " + other.stringify());
	}
	
	public BaseType getAddedType(StringType other) {
		throw new RuntimeException("Cannot add " + stringify() + " to " + other.stringify());
	}
	
	public BaseType getAddedType(IntegerType other) {
		throw new RuntimeException("Cannot add " + stringify() + " to " + other.stringify());
	}
	
	public boolean isAssignableFrom(BaseType other) {
		if(!isTypeAssignable()) {
			return false;
		} else {
			return isEqualityComparableTo(other);
		}
	}
	
	public boolean isTypeAssignable() {
		return true;
	}
	
	public boolean isEqualityComparableTo(BaseType other) {
		return false;
	}
	
	public boolean isEqualityComparableTo(ArrayType other) {
		return false;
	}
	
	public boolean isEqualityComparableTo(BoolType other) {
		return false;
	}
	
	public boolean isEqualityComparableTo(IntegerType other) {
		return false;
	}
	
	public boolean isEqualityComparableTo(FunctionType other) {
		return false;
	}
	
	public boolean isEqualityComparableTo(StringType other) {
		return false;
	}
	
	public boolean isEqualityComparableTo(TupleType other) {
		return false;
	}
	
	public boolean isEqualityComparableTo(VersionType other) {
		return false;
	}
	
	public boolean isEqualityComparableTo(ObjectType other) {
		return false;
	}
	
	public boolean isEqualityComparableTo(NullType other) {
		return true;
	}

}
