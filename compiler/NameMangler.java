
public class NameMangler {
	
	private static NameMangler nameMangler;
	
	public static NameMangler getInstance() {
		if(nameMangler == null) {
			synchronized(NameMangler.class) {
				if(nameMangler == null) {
					nameMangler = new NameMangler();
				}
			}
		}
		return nameMangler;
	}
	
	long i = 0;
	public String getTempVarName() {
		return "_temp_" + i++;
	}
	
	public static String mangle(String name) {
		if(name.startsWith("_")) {
			return "_" + name;
		} else {
			return name;
		}
	}
}
