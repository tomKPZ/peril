import java.util.ArrayList;
import java.util.List;

public class StatementList implements AstNode {
	
	private List<ClassDeclaration> classDeclarations = new ArrayList<ClassDeclaration>();
	private List<Statement> statements = new ArrayList<Statement>();
	
	public StatementList() {}
	
	public void add(Statement statement) {
		if(statement.isClassDeclaration()) {
			classDeclarations.add((ClassDeclaration)statement);
		} else {
			statements.add(statement);
		}
	}
	
	public StringBuilder stringify () {
		StringBuilder sb = new StringBuilder();
		for(Statement d : classDeclarations) {
			sb.append(d.stringify()).append("\n");
		}
		for(Statement s : statements) {
			sb.append(s.stringify()).append("\n");
		}
		return sb;
	}

	@Override
	public StringBuilder transpile() {
		StringBuilder sb = new StringBuilder();
		for(ClassDeclaration cd : classDeclarations) {
			sb.append(cd.getForwardDeclaration()).append("\n");
		}
		for(ClassDeclaration cd : classDeclarations) {
			sb.append(cd.getDeclaration()).append("\n");
		}
		for(ClassDeclaration cd : classDeclarations) {
			sb.append(cd.getInitializer()).append("\n");
		}
		for(Statement statement : statements) {
			sb.append(statement.transpile()).append("\n");
		}
		return sb;
	}
	
	@Override
	public void typeCheck() {
		for(ClassDeclaration cd : classDeclarations) {
			Scope.getInstance().addUserType(cd);
		}
		for(ClassDeclaration cd : classDeclarations) {
			cd.addVariableTypes();
		}
		for(ClassDeclaration cd : classDeclarations) {
			cd.typeCheckDefinition();
		}
		for(Statement statement : statements) {
			statement.typeCheck();
		}
	}
	
}
