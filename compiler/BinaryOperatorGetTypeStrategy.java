
public abstract class BinaryOperatorGetTypeStrategy {
	public abstract BaseType binaryGetType(BaseType t1, BaseType t2);
}