
public class BinaryOperatorPlus extends BinaryOperator {

	public BinaryOperatorPlus(Expression e1, Expression e2) {
		super(e1, "+", e2);
	}

	@Override
	protected String getTranspiledOpString() {
		return "+";
	}

	@Override
	protected BinaryOperatorTypeCheckStrategy getTypeCheckStrategy() {
		return new AddableTypeCheckStrategy();
	}

	@Override
	protected BinaryOperatorGetTypeStrategy getGetTypeStrategy() {
		return new AddableGetTypeStrategy();
	}

}
