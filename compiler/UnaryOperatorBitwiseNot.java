
public class UnaryOperatorBitwiseNot extends UnaryOperator {

	UnaryOperatorBitwiseNot(Expression e) {
		super("~", e);
	}

	@Override
	protected String getTranspiledOpString() {
		return "~";
	}

	@Override
	protected void unaryTypeCheck() {
		if(!e.getType().isIntegralValue()) {
			throw new RuntimeException("Cannot take the bitwise negation of a non-integer type " + e.getType().stringify());
		}
	}

}
