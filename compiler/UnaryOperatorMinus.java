
public class UnaryOperatorMinus extends UnaryOperator {

	UnaryOperatorMinus(Expression e) {
		super("-", e);
	}

	@Override
	protected String getTranspiledOpString() {
		return "-";
	}

	@Override
	protected void unaryTypeCheck() {
		if(!e.getType().isNumeric()) {
			throw new RuntimeException("Cannot negate non-numeric type " + e.getType().stringify());
		}
	}

}
