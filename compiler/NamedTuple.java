import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NamedTuple {
	
	public abstract class Field {
		public abstract boolean hasName();
		public abstract QualifiedType getType();
		public abstract String getName();
		public abstract StringBuilder stringify();
		public abstract StringBuilder transpile();
		public abstract void typeCheck();
		public abstract boolean isCompatibleWith(BaseType type);
	}
	
	public class NamedField extends Field {
		
		public QualifiedType type;
		public String name;
		
		public NamedField(QualifiedType type, String name) {
			this.type = type;
			this.name = name;
		}
		
		@Override
		public QualifiedType getType() {
			return type;
		}
		
		@Override
		public StringBuilder stringify() {
			return type.stringify().append(" ").append(name);
		}

		@Override
		public boolean hasName() {
			return true;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public void typeCheck() {
			type.typeCheck();
		}

		@Override
		public boolean isCompatibleWith(BaseType type) {
			return this.type.baseType.isAssignableFrom(type);
		}

		@Override
		public StringBuilder transpile() {
			return type.baseType.transpile().append(" _").append(name);
		}
	}
	
	public class IgnoredField extends Field {
		@Override
		public QualifiedType getType() {
			throw new RuntimeException("Can't get type from a _ field");
		}

		@Override
		public StringBuilder stringify() {
			return new StringBuilder("_");
		}

		@Override
		public boolean hasName() {
			return false;
		}

		@Override
		public String getName() {
			throw new RuntimeException("Can't get name from a _ field");
		}

		@Override
		public void typeCheck() {}

		@Override
		public boolean isCompatibleWith(BaseType type) {
			return true;
		}

		@Override
		public StringBuilder transpile() {
			throw new RuntimeException("Cannot transpile an ignored field");
		}
	}
	
	public List<Field> fields;
	public Set<String> names;
	
	public NamedTuple() {
		fields = new ArrayList<Field>();
		names = new HashSet<String>();
	}
	
	protected int nNamed = 0;
	
	public void addField(QualifiedType type, String name) {
		if(names.contains(name)) {
			throw new RuntimeException("duplicate name in structure");
		}
		nNamed++;
		fields.add(new NamedField(type, name));
		names.add(name);
	}
	
	public void addIgnoredField() {
		fields.add(new IgnoredField());
	}
	
	public boolean isCompatibleWith(BaseType type) {
		if(type.nSubtypes() != fields.size()) {
			return false;
		}
		for(int i = 0; i < fields.size(); i++) {
			if(!fields.get(i).isCompatibleWith(type.getSubtype(i))) {
				return false;
			}
		}
		return true;
	}
	
	public BaseType toBaseType() {
		if(fields.size() == 1) {
			return fields.get(0).getType().baseType;
		} else {
			TupleType t = new TupleType();
			for(int i = 0; i < fields.size(); i++) {
				t.addType(fields.get(i).getType().baseType);
			}
			return t;
		}
	}

	public StringBuilder stringify() {
		StringBuilder sb = new StringBuilder("(");
		for(int i = 0; i < fields.size(); i++) {
			Field field = fields.get(i);
			sb.append(field.stringify());
			if(i != fields.size() - 1) {
				sb.append(", ");
			}
		}
		return sb.append(")");
	}
	
	public StringBuilder transpile() {
		StringBuilder sb = new StringBuilder("(");
		for(int i = 0; i < fields.size(); i++) {
			Field field = fields.get(i);
			sb.append(field.transpile());
			if(i != fields.size() - 1) {
				sb.append(", ");
			}
		}
		return sb.append(")");
	}

	public void typeCheck() {
		for(Field field : fields) {
			field.typeCheck();
		}
	}
	
	public int nNamedFields() {
		return nNamed;
	}
	
}
