
public class BooleanGetTypeStrategy extends BinaryOperatorGetTypeStrategy {

	@Override
	public BaseType binaryGetType(BaseType t1, BaseType t2) {
		return new BoolType();
	}

}
