
public class ForLoop extends Statement {
	
	protected Statement initializer, body;
	protected Expression condition, iterator;
	
	public ForLoop(Statement initializer, Expression condition, Expression iterator, Statement body) {
		this.initializer = initializer;
		this.condition = condition;
		this.iterator = iterator;
		this.body = body;
	}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("for (")
				.append(initializer.stringify()).append(" ")
				.append(condition.stringify()).append("; ")
				.append(iterator.stringify()).append(") {\n")
				.append(body.stringify()).append("}");
	}

	@Override
	public StringBuilder transpile() {
		StringBuilder sb = new StringBuilder();
		if(initializer.nTranspiledStatements() == 1) {
			sb.append("for (").append(initializer.transpile()).append(" ");
		} else {
			sb.append("{\n").append(initializer.transpile()).append("\nfor (;");
		}
		sb.append(condition.transpile()).append("; ")
				.append(iterator.transpile()).append(") {\n")
				.append(body.transpile()).append("}");
		if(initializer.nTranspiledStatements() != 1) {
			sb.append("\n}");
		}
		return sb;
	}

	@Override
	public void typeCheck() {
		Scope.getInstance().enterScope();
		initializer.typeCheck();
		condition.typeCheck();
		if(!condition.getType().isConditional()) {
			throw new RuntimeException("Condition " + condition.stringify() + " in for loop was of type " + condition.getType().stringify() + ", not bool");
		}
		iterator.typeCheck();
		Scope.getInstance().enterLoop();
		body.typeCheck();
		Scope.getInstance().leaveScope();
		Scope.getInstance().leaveScope();
	}
}
