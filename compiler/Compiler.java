import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CommonTokenStream;

public class Compiler {
	public static String readStream(Reader r) {
	    StringBuilder sb = new StringBuilder();
	    try {
	        int c = 0;
	        while ((c = r.read()) != -1) {
	            sb.append((char) c);
	        }
	    } catch (IOException e) {
	        throw new RuntimeException(e);
	    }
	    return sb.toString();
	}
	
	public static void main(String[] arguments) throws Exception {
		if(arguments.length != 2) {
			throw new RuntimeException("Usage: java Compiler [PERIL file] [skeleton file]");
		}
		// Create a reader for our input file
		Reader programReader = new BufferedReader(new FileReader(new File(arguments[0])));
		
		// parse our program
		ANTLRInputStream in = new ANTLRInputStream(programReader);
		PerilLexer lexer = new PerilLexer(in);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		PerilParser parser = new PerilParser(tokens);
		parser.setErrorHandler(new BailErrorStrategy());
		
		// transpile our program
		AstNode program = parser.eval().value;
		program.typeCheck();
		StringBuilder compiled = PrettyFormatter.format(program.transpile(), 1);
		
		// Create a reader for our skeleton file
		Reader skeletonReader = new BufferedReader(new FileReader(new File(arguments[1])));
		String skeleton = readStream(skeletonReader);
		
		// Write out
		System.out.print(skeleton.replace("$PROGRAM\n", compiled.toString()));
	}
}
