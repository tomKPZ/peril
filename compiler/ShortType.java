
public class ShortType extends IntegerType {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("short");
	}

	@Override
	public int bitWidth() {
		return 16;
	}
	
}
