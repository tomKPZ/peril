
public class IfElseStatement extends Statement {
	private Expression condition;
	private Statement trueStatement, falseStatement;
	public IfElseStatement(Expression condition, Statement trueStatement, Statement falseStatement) {
		this.condition = condition;
		this.trueStatement = trueStatement;
		this.falseStatement = falseStatement;
	}
	@Override
	public StringBuilder stringify() {
		return new StringBuilder("if (").append(condition.stringify()).append(") ").append(trueStatement.stringify()).append(" else ").append(falseStatement.stringify());
	}
	@Override
	public StringBuilder transpile() {
		return new StringBuilder("if (").append(condition.transpile()).append(") {\n").append(trueStatement.transpile()).append("\n} else {\n").append(falseStatement.transpile()).append("\n}");
	}
	@Override
	public void typeCheck() {
		condition.typeCheck();
		if(!condition.getType().isConditional()) {
			throw new RuntimeException("Condition " + condition.stringify() + " in for loop was of type " + condition.getType().stringify() + ", not bool");
		}
		Scope.getInstance().enterScope();
		trueStatement.typeCheck();
		falseStatement.typeCheck();
		Scope.getInstance().leaveScope();
	}
}
