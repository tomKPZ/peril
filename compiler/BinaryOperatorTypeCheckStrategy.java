
public abstract class BinaryOperatorTypeCheckStrategy {
	public abstract void binaryTypeCheck(BaseType t1, BaseType t2);
}