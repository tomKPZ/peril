
public class FunctionCall extends Expression {
	
	protected Expression f, arg;
	
	public FunctionCall(Expression f, Expression arg) {
		this.f = f;
		this.arg = arg;
	}
	
	@Override
	public StringBuilder stringify() {
		return new StringBuilder("(").append(f.stringify()).append(") (").append(arg.stringify()).append(")");
	}
	
	@Override
	public StringBuilder transpile() {
		if(f.getType().getArgumentsType().nSubtypes() == 1) {
			return new StringBuilder("(").append(f.transpile()).append(")(").append(arg.transpile()).append(")");
		} else {
			return new StringBuilder("_funcCall(").append(f.transpile()).append(", ").append(arg.transpile()).append(")");
		}
	}
	
	@Override
	public BaseType getTypeImpl() {
		return f.getType().getReturnType();
	}
	
	@Override
	public void typeCheck() {
		f.typeCheck();
		arg.typeCheck();
		if(!f.getType().isCallable()) {
			throw new RuntimeException("Cannot call non-function \"" + f.stringify() + "\" of type " + f.getType().stringify());
		}
		if(!f.getType().getArgumentsType().isAssignableFrom(arg.getType())) {
			throw new RuntimeException("Cannot convert " + arg.getType().stringify() + " to " + f.getType().getArgumentsType().stringify() + " for function call to " + f.stringify());
		}
	}
}
