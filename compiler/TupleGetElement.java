
public class TupleGetElement extends Expression {
	
	private Expression exp;
	private int index;
	public TupleGetElement(Expression exp, int index) {
		this.exp = exp;
		this.index = index;
	}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("(").append(exp.stringify()).append(")[").append(index).append("]");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("_tupleGet<").append(exp.getType().transpile()).append(", ").append(index).append(">(").append(exp.transpile()).append(")");
	}

	@Override
	public void typeCheck() {
		exp.typeCheck();
	}

	@Override
	public BaseType getTypeImpl() {
		return exp.getType().getSubtype(index);
	}

}
