
public class UnaryOperatorLogicalNot extends UnaryOperator {

	UnaryOperatorLogicalNot(Expression e) {
		super("not", e);
	}

	@Override
	protected String getTranspiledOpString() {
		return "!";
	}

	@Override
	protected void unaryTypeCheck() {
		if(!e.getType().isConditional()) {
			throw new RuntimeException("Cannot take the logical negation of non-boolean type " + e.getType().stringify());
		}
	}

}
