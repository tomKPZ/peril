
public class ObjectType extends BaseType {
	
	private String name;
	private ClassDeclaration cd;
	
	public ObjectType(String name) {
		this.name = name;
		cd = null;
	}
	
	@Override
	public ClassDeclaration getClassDeclaration() {
		return cd;
	}
	
	public String getTypeName() {
		return name;
	}
	
	@Override
	public StringBuilder stringify() {
		return new StringBuilder(name);
	}
	
	@Override
	public StringBuilder transpile() {
		return new StringBuilder(NameMangler.mangle(name)).append(" *");
	}
	
	@Override
	public void typeCheck() {
		cd = Scope.getInstance().getClassDeclaration(name);
	}
	
	@Override
	public Expression getDefaultValue() {
		return new NullExpression();
	}
	
	@Override
	public StringBuilder transpileNewExpression() {
		return new StringBuilder("_new_").append(getTypeName()).append("()");
	}
	
	@Override
	public BaseType getMemberType(String member) {
		return getClassDeclaration().getFieldType(member).baseType;
	}
	
	@Override
	public boolean isEqualityComparableTo(BaseType other) {
		return other.isEqualityComparableTo(this);
	}
	
	@Override
	public boolean isEqualityComparableTo(ObjectType other) {
		return name.equals(other.name);
	}
	
	@Override
	public boolean isEqualityComparableTo(NullType other) {
		return true;
	}
	
}
