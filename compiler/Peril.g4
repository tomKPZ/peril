grammar Peril;

/* This will be the entry point of our parser. */
eval returns [StatementList value]
    :    l=statementList EOF { $value = $l.value; } 
    ;

statementList returns [StatementList value]
    :    { $value = new StatementList(); }
         ( s=statement { $value.add($s.value); }
         )*
    ;

/* Some statements may not be terminated by a semicolon */
statement returns [Statement value]
    :    vd=variableDeclaration { $value = $vd.value; }
    |    'return' e=expression ';' { $value = new ReturnStatement($e.value); }
    |    'return' ';' { $value = new ReturnStatement(new TupleExpression()); }
    |    cd=classDeclaration { $value = $cd.value; }
    |    'if' '(' e=expression ')' s=statement { $value = new IfStatement($e.value, $s.value); }
    |    'if' '(' e=expression ')' s1=statement 'else' s2=statement { $value = new IfElseStatement($e.value, $s1.value, $s2.value); }
    |    'for' '(' s1=statement e1=expression ';' e2=expression ')' s2=statement {
             $value = new ForLoop($s1.value, $e1.value, $e2.value, $s2.value);
         }
    |    'while' '(' e=expression ')' s=statement { $value = new WhileLoop($e.value, $s.value); }
    |    '{' sl=statementList '}' { $value = new CodeBlock($sl.value); }
    |    e=expression ';' { $value = new ExprStatement($e.value); }
    |    'print' e=expression ';' { $value = new PrintStatement($e.value, false); }
    |    'println' e=expression ';' { $value = new PrintStatement($e.value, true); }
    |    'break' ';' { $value = new BreakStatement(); }
    |    'continue' ';' { $value = new ContinueStatement(); }
    |    'pass' ';' { $value = new EmptyStatement(); } // must be explicit if we want an empty statement
    ;

variableDeclaration returns [ClassMemberDeclaration value]
    :    ql=typeQualifierList ft=functionType i=Identifier '=' t=namedTuple '->' tbArg=typeBase '{' sl=statementList '}' ';'? {
             $value = new NamedFunctionStatement($ql.value, $ft.value, $i.text, $t.value, $tbArg.value, $sl.value);
         }
    |    ql=typeQualifierList tb=typeBase i=Identifier e=variableInitExpressionTail ';' {
             NamedTuple nt = new NamedTuple();
             nt.addField(new QualifiedType($ql.value, $tb.value), $i.text);
             if($e.value == null) {
                $value = new VarInitStatement(nt, null);
             } else {
                TupleExpression te = new TupleExpression();
                te.addExpression($e.value);
                $value = new VarInitStatement(nt, te);
             }
         }
    |    t=namedTuple e=variableInitExpressionTail ';' { $value = new VarInitStatement($t.value, $e.value); }
    ;

variableInitExpressionTail returns [Expression value]
    :    '=' e=expression { $value = $e.value; }
    |    { $value = null; }
    ;

classDeclaration returns [ClassDeclaration value]
    :    ('struct' | 'class') i1=Identifier '{' { $value = new ClassDeclaration($i1.text); }
         (vd=variableDeclaration                { $value.addField($vd.value); })*
         '}' ';'? // optional semicolon
    ;
    
qualifiedType returns [QualifiedType value]
    :    '(' t=qualifiedType ')'         { $value = $t.value; }
    |    ql=typeQualifierList b=typeBase { $value = new QualifiedType($ql.value, $b.value); }
    ;
    
typeQualifierList returns [QualifierList value]
    :    { $value = new QualifierList(); }
         ( q=TypeQualifier { $value.addQualifier($q.text); } )*
    ;

typeBase returns [BaseType value]
    :    'var'                           { $value = null; }
    |    ft=functionType                 { $value = $ft.value; }
    |    t=atomType                      { $value = $t.value; }
    |    '[' qt=qualifiedType ']'        { $value = new ArrayType($qt.value); }
    ;

functionType returns [FunctionType value]
    :    args=atomType '->' ret=typeBase { $value = new FunctionType($args.value, $ret.value); }
    |    'var'                           { $value = null; }
    ;

atomType returns [BaseType value]
    :    'byte'             { $value = new ByteType(); }
    |    'short'            { $value = new ShortType(); }
    |    'int'              { $value = new IntType(); }
    |    'long'             { $value = new LongType(); }
    |    'bool'             { $value = new BoolType(); }
    |    'Version'          { $value = new VersionType(); }
    |    'String'           { $value = new StringType(); }
    |    '(' p=typeBase ')' { $value = $p.value; }
    |    t=tupleType        { $value = $t.value; }
    |    n=Identifier       { $value = new ObjectType($n.text); }
    ;
    
tupleType returns [TupleType value]
    :    '(' { $value = new TupleType(); }
         (t=typeBase ','? { $value.addType($t.value); } )* /* commas separating types are optional */
         ')'
    ;
    
expression returns [Expression value]
    :    exp=assignment { $value = $exp.value; }
    ;

/* Named tuples are only used in two places: function argument lists, and naming multiple
   variables at once by assigning it to a single tuple.  They are not real types.
   (int i, _, int j) = (1,2,3);
 */
namedTuple returns [NamedTuple value]
    :    t=qualifiedType i=Identifier { $value = new NamedTuple(); $value.addField($t.value, $i.text); }
    |    '(' { $value = new NamedTuple(); }
         ( ((t=qualifiedType i=Identifier { $value.addField($t.value, $i.text); })
            | ('_' { $value.addIgnoredField(); })) ','? )*
         ')'
    ;
    
assignment returns [Expression value]
    :    e1=orTest '=' e2=assignment { $value = new SetVarExp($e1.value, $e2.value); }
    |    o=orTest { $value = $o.value; }
    ;
    
orTest returns [Expression value]
    :    a=andTest 'or' o=orTest { $value = new BinaryOperatorLogicalOr($a.value, $o.value); }
    |    a=andTest { $value = $a.value; }
    ;
    
andTest returns [Expression value]
    :    n=notTest 'and' a=andTest { $value = new BinaryOperatorLogicalAnd($n.value, $a.value); }
    |    n=notTest { $value = $n.value; }
    ;

notTest returns [Expression value]
    :    'not' n=notTest { $value = new UnaryOperatorLogicalNot($n.value); }
    |    c=comparison { $value = $c.value; }
    ;

comparison returns [Expression value]
    :    e1=orExp o=('>'|'<'|'>='|'<='|'=='|'!=') e2=comparison { $value = new Comparison($e1.value, $o.text, $e2.value); }
    |    e=orExp                                                { $value = $e.value; }
    ;
    
orExp returns [Expression value]
    :    e1=orExp '|' e2=xorExp { $value = new BinaryOperatorBitwiseOr($e1.value, $e2.value); }
    |    e=xorExp               { $value = $e.value; }
    ;

xorExp returns [Expression value]
    :    e1=xorExp '^' e2=andExp { $value = new BinaryOperatorBitwiseXor($e1.value, $e2.value); }
    |    e=andExp                { $value = $e.value; }
    ;

andExp returns [Expression value]
    :    e1=andExp '&' e2=shiftExp { $value = new BinaryOperatorBitwiseAnd($e1.value, $e2.value); }
    |    e=shiftExp                { $value = $e.value; }
    ;

shiftExp returns [Expression value]
    :    e1=shiftExp '<<'  e2=arithExp { $value = new BinaryOperatorLeftShift($e1.value, $e2.value); }
    |    e1=shiftExp '>>'  e2=arithExp { $value = new BinaryOperatorArithmeticRightShift($e1.value, $e2.value); }
    |    e1=shiftExp '>>>' e2=arithExp { $value = new BinaryOperatorLogicalRightShift($e1.value, $e2.value); }
    |    e=arithExp                    { $value = $e.value; }
    ;

arithExp returns [Expression value]
    :    e1=arithExp '+' e2=term { $value = new BinaryOperatorPlus($e1.value, $e2.value); }
    |    e1=arithExp '-' e2=term { $value = new BinaryOperatorMinus($e1.value, $e2.value); }
    |    e=term                  { $value = $e.value; }
    ;

term returns [Expression value]
    :    e1=term '*' e2=factor { $value = new BinaryOperatorTimes($e1.value, $e2.value); }
    |    e1=term '/' e2=factor { $value = new BinaryOperatorDivide($e1.value, $e2.value); }
    |    e1=term '%' e2=factor { $value = new BinaryOperatorMod($e1.value, $e2.value); }
    |    e=factor              { $value = $e.value; }
    ;
    
factor returns [Expression value]
    :    '+' f=factor  { $value = $f.value; }
    |    '-' f=factor  { $value = new UnaryOperatorMinus($f.value); }
    |    '~' f=factor  { $value = new UnaryOperatorBitwiseNot($f.value); }
    |    e=arrayAccess { $value = $e.value; }
    ;
    
arrayAccess returns [Expression value]
    :    a=arrayAccess '[' e=expression ']' { $value = new ArrayLookup($a.value, $e.value); }
    |    m=functionCallOrMemberAccess       { $value = $m.value; }
    ;

functionCallOrMemberAccess returns [Expression value]
    :    e1=functionCallOrMemberAccess e2=atom                 { $value = new FunctionCall($e1.value, $e2.value); }
    |    a=functionCallOrMemberAccess '.' n=Identifier         { $value = new MemberAccessExpression($a.value, $n.text); }
    |    e=atom                                                { $value = $e.value; }
    ;
    
atom returns [Expression value]
    :    'null' { $value = new NullExpression(); }
    |    'true' { $value = new TrueExpression(); }
    |    'false' { $value = new FalseExpression(); }
    |    'new' t=typeBase { $value = new NewExpression($t.value); }
    |    i=Integer { $value = new IntegerConstant($i.text); }
    |    nt=namedTuple '->' t=typeBase '{' s=statementList '}' { $value = new Function($nt.value, $t.value, $s.value); }
    // A single expression in parenthesis will not expand to a TupleExpression, although it could
    |    '(' exp=expression ')' { $value = $exp.value; }
    |    te=tupleExpression { $value = $te.value; }
    |    i=Identifier { $value = new VarLookup($i.text); }
    |    str=StringLiteral { $value = new StringLiteral($str.text); }
    ;
    
tupleExpression returns [TupleExpression value]
    :    '(' ')' { $value = new TupleExpression(); }
    |    '(' { $value = new TupleExpression(); }
         (e1=expression ',' { $value.addExpression($e1.value); } )+
         (e2=expression { $value.addExpression($e2.value); } )
         ')'
    ;
    
StringLiteral
    :    UnterminatedStringLiteral '"'
    ;

UnterminatedStringLiteral
    :    '"' (~["\\\r\n] | '\\' (. | EOF))*
    ;

TypeQualifier
    :    'transient'
    |    'persistent'
    ;

Identifier
    // a variable cannot be named '_'
    :    ('_' ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*)
    |    (('a'..'z' | 'A'..'Z') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*)
    ;

Integer
    :    ('0'..'9')+
    ;

Decimal
    :    ('0'..'9')+ ('.' ('0'..'9')+)?
    ;

Comment
    :    (('//' .*? ('\n' | EOF))
    |     ('/*' .*? '*/')) -> skip
    ;

/* We're going to ignore all white space characters */
WS
    :    (' ' | '\t' | '\r'| '\n') -> skip
    ;

// handle characters which failed to match any other token
ErrorCharacter : . ;