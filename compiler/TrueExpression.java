
public class TrueExpression extends Expression {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("true");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("true");
	}

	@Override
	public void typeCheck() {}

	@Override
	public BaseType getTypeImpl() {
		return new BoolType();
	}

}
