
public class FalseExpression extends Expression {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("false");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("false");
	}

	@Override
	public void typeCheck() {}

	@Override
	public BaseType getTypeImpl() {
		return new BoolType();
	}

}
