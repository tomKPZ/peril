
public class StringType extends BaseType {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("String");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("std::string");
	}

	@Override
	public void typeCheck() {
		
	}

	@Override
	public Expression getDefaultValue() {
		return new StringLiteral("\"\"");
	}
	
	@Override
	public boolean isAddableTo(BaseType other) {
		return other.isAddableTo(this);
	}
	
	@Override
	public boolean isAddableTo(StringType other) {
		return true;
	}
	
	@Override
	public BaseType getAddedType(BaseType other) {
		return other.getAddedType(this);
	}
	
	@Override
	public BaseType getAddedType(StringType other) {
		return new StringType();
	}
	
	@Override
	public boolean isEqualityComparableTo(BaseType other) {
		return other.isEqualityComparableTo(this);
	}
	
	@Override
	public boolean isEqualityComparableTo(StringType other) {
		return true;
	}

}
