
public class ArrayLookup extends Expression {
	
	private Expression arr, index;
	
	public ArrayLookup(Expression arr, Expression index) {
		this.arr = arr;
		this.index = index;
	}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("(").append(arr.stringify()).append(")[").append(index.stringify()).append("]");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("(*(").append(arr.transpile()).append("))[").append(index.transpile()).append("]");
	}

	@Override
	public void typeCheck() {
		arr.typeCheck();
		if(!arr.getType().isIndexable()) {
			throw new RuntimeException("Cannot lookup element in non-array \"" + arr.stringify() + "\" of type " + arr.getType().stringify());
		}
		index.typeCheck();
		if(!index.getType().isIntegralValue()) {
			throw new RuntimeException("Cannot lookup element in array with non-integral index \"" + index.stringify() + "\" of type " + index.getType().stringify());
		}
	}

	@Override
	public BaseType getTypeImpl() {
		return arr.getType().getIndexedType().baseType;
	}
	
	@Override
	public boolean isExpressionAssignable() {
		return true;
	}

}
