
public class ContinueStatement extends Statement {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("continue;");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("continue;");
	}

	@Override
	public void typeCheck() {
		if(!Scope.getInstance().isInLoop()) {
			throw new RuntimeException("Must be in a loop to continue");
		}
	}

}
