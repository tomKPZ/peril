
public class LongType extends IntegerType {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("long");
	}
	
	@Override
	public int bitWidth() {
		return 64;
	}
	
}
