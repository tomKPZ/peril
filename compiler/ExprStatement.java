
public class ExprStatement extends Statement {
	private Expression exp;
	public ExprStatement(Expression exp) {
		this.exp = exp;
	}
	
	public StringBuilder stringify() {
		return exp.stringify().append(";");
	}

	@Override
	public StringBuilder transpile() {
		return exp.transpile().append(";");
	}

	@Override
	public void typeCheck() {
		exp.typeCheck();
	}
}
