#include <algorithm>
#include <cstddef>
#include <functional>
#include <iostream>
#include <tuple>
#include <unordered_map>
#include <utility>

#include "persistent-field.hpp"

#if defined(NDEBUG)
#include <dlfcn.h>
#endif

template <typename T>
class PersistentField {
 public:
  template <typename... S>
  constexpr auto operator()(S... s) const {
    return (lookup(now))(s...);
  }

  PersistentField(const PersistentField& other) {
    update(now, other.lookup(now));
  }

  PersistentField(const T& value) { update(now, value); }

  PersistentField<T>& operator=(const T& value) {
    update(now, value);
    return *this;
  }

  PersistentField<T>& operator=(const PersistentField<T>& field) {
    update(now, field.lookup(now));
    return *this;
  }

  T operator->() const { return lookup(now); }

  operator const T&() const { return lookup(now); }

  const T& lookup(Version version) const { return field.get(version); }

  void update(Version version, const T& value) {
    now = field.set(value, version);
  }

 private:
  PersistentFieldImpl<T> field;
};

template <typename T>
struct SparseArray {
  inline T& operator[](int64_t index) { return array[index]; }

 private:
  std::unordered_map<int64_t, T> array;
};

template <typename T>
struct SparseArray<PersistentField<T>> {
  inline PersistentField<T>& operator[](int64_t index) {
    auto it = array.find(index);
    if (it != array.end()) {
      return it->second;
    }
    return array.emplace_hint(it, index, T())->second;
  }

 private:
  std::unordered_map<int64_t, PersistentField<T>> array;
};

template <typename T>
struct FieldLookupHelper {
  static constexpr T fieldLookup(const T& t) { return t; }
};

template <typename T>
struct FieldLookupHelper<PersistentField<T>> {
  static constexpr T fieldLookup(const PersistentField<T>& t) {
    return t.lookup(now);
  }
};

template <typename T>
static constexpr auto _fieldLookup(const T& t) {
  return FieldLookupHelper<T>::fieldLookup(std::forward<const T>(t));
}

template <typename T, std::size_t S>
struct TupleGetHelper {
  static constexpr auto& tupleGet(const T& t) { return std::get<S>(t); }
};

template <typename T, std::size_t S>
struct TupleGetHelper<PersistentField<T>, S> {
  static constexpr auto& tupleGet(const PersistentField<T>& t) {
    return std::get<S>(t.lookup(now));
  }
};

template <typename T, std::size_t S>
static constexpr auto& _tupleGet(const T& t) {
  return TupleGetHelper<T, S>::tupleGet(std::forward<const T&>(t));
}

template <typename T>
static inline constexpr T& _deref(T& x) {
  return x;
}

template <typename T>
static inline constexpr T& _deref(T* x) {
  return *x;
}

// Credit for gen_seq to Xeo's answer to
// http://stackoverflow.com/questions/17424477/implementation-c14-make-integer-sequence

// using aliases for cleaner syntax
template <class T>
using Invoke = typename T::type;

template <std::size_t...>
struct seq {
  using type = seq;
};

template <class S1, class S2>
struct concat;

template <std::size_t... I1, std::size_t... I2>
struct concat<seq<I1...>, seq<I2...>> : seq<I1..., (sizeof...(I1) + I2)...> {};

template <class S1, class S2>
using Concat = Invoke<concat<S1, S2>>;

template <std::size_t N>
struct gen_seq;
template <std::size_t N>
using GenSeq = Invoke<gen_seq<N>>;

template <std::size_t N>
struct gen_seq : Concat<GenSeq<N / 2>, GenSeq<N - N / 2>> {};

template <>
struct gen_seq<0> : seq<> {};
template <>
struct gen_seq<1> : seq<0> {};

// Credit for _apply_tuple_impl and _apply_from_tuple to
// https://github.com/sftrabbit/CppSamples-Samples/blob/master/1-common-tasks/functions/apply-tuple-to-function.cpp
template <typename F, typename Tuple, std::size_t... S>
constexpr auto _apply_tuple_impl(const F& fn, const Tuple& t, seq<S...>) {
  return std::forward<const F>(fn)(
      std::get<S>(std::forward<const Tuple>(t))...);
}

template <typename F, typename Tuple>
constexpr auto _apply_from_tuple(const F& fn, const Tuple& t) {
  return _apply_tuple_impl(
      std::forward<const F>(fn), std::forward<const Tuple>(t),
      GenSeq<std::tuple_size<
          typename std::remove_reference<Tuple>::type>::value>());
}

template <typename F, typename... A>
struct FuncCallHelper {
  static constexpr auto funcCall(const F& f, const A&... a) { return f(a...); }
};

template <typename F, typename... A>
struct FuncCallHelper<F, std::tuple<A...>> {
  static constexpr auto funcCall(const F& f, const std::tuple<A...>& a) {
    return _apply_from_tuple(f, a);
  }
};

template <typename F, typename... A>
static constexpr auto _funcCallImpl(const F& f, const A&... a) {
  return FuncCallHelper<F, A...>::funcCall(std::forward<const F>(f), a...);
}

template <typename F, typename A>
constexpr auto _funcCall(const F& f, const A& a) {
  return _funcCallImpl(_fieldLookup(f), _fieldLookup(a));
}

template <typename T, typename S>
constexpr T _logicalRightShiftImpl(T n, S shamt) {
  return ((typename std::make_unsigned<T>::type)n) >> shamt;
}

template <typename T, typename S>
constexpr auto _logicalRightShift(T n, S shamt) {
  return _logicalRightShiftImpl(_fieldLookup(n), _fieldLookup(shamt));
}

template <typename T>
std::ostream& operator<<(std::ostream& os, PersistentField<T> const& field) {
  return os << field.lookup(now);
}

// The standard memory allocator is useful for debugging.
// This custom one gives better performance, so it is
// enabled for profiling and release builds.
#if defined(NDEBUG)
static char* heap = []() {
  return reinterpret_cast<char*>(malloc(1024 * 1024 * 1024));
}();
void* operator new(size_t count) {
  constexpr int align = sizeof(long) * 2;
  count += (align - (count % align)) % align;
  heap += count;
  return heap - count;
}
__attribute__((visibility("hidden")))
void operator delete(void*) noexcept {}
__attribute__((visibility("hidden")))
void operator delete(void*, std::size_t) noexcept {}
#endif

int main(void) {
$PROGRAM
  return 0;
}
