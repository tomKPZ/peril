
public class QualifiedType implements AstNode {
	public QualifierList qualifierList;
	public BaseType baseType;
	public QualifiedType(QualifierList qualifierList, BaseType baseType) {
		this.qualifierList = qualifierList;
		this.baseType = baseType;
	}

	@Override
	public StringBuilder stringify() {
		StringBuilder sb = new StringBuilder();
		sb.append(qualifierList.toString());
		if(sb.length() > 0) {
			sb.append(" ");
		}
		if(baseType == null) {
			return sb.append("var");
		} else {
			return sb.append(baseType.stringify());
		}
	}

	@Override
	public StringBuilder transpile() {
		if(qualifierList.isPersistent()) {
			return new StringBuilder("PersistentField<").append(baseType.transpile()).append(">");
		} else {
			return new StringBuilder(baseType.transpile());
		}
	}

	@Override
	public void typeCheck() {
		if(baseType != null) {
			baseType.typeCheck();
		}
	}
}
