
public class NullExpression extends Expression {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("null");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("nullptr");
	}

	@Override
	public void typeCheck() {}

	@Override
	public BaseType getTypeImpl() {
		return new NullType();
	}
	
}
