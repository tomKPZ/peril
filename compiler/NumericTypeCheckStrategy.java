
public class NumericTypeCheckStrategy extends BinaryOperatorTypeCheckStrategy {
	
	@Override
	public void binaryTypeCheck(BaseType t1, BaseType t2) {
		if(!t1.isNumeric() || !t2.isNumeric()) {
			throw new RuntimeException("Binary numeric operator expected numeric arguments, but arguments were of types " + t1.stringify() + " and " + t2.stringify());
		}
	}
	
}
