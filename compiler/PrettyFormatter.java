import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class PrettyFormatter {
	
	protected static class StringWithLiterals {
		public String s;
		protected boolean[] inLiteral;
		
		public StringWithLiterals(String s) {
			this.s = s;
			inLiteral = new boolean[s.length()];
			
			boolean currentlyInLiteral = false;
			
			for(int i = 0; i < s.length(); inLiteral[i++] = currentlyInLiteral) {
				char c = s.charAt(i);
				if(currentlyInLiteral && c == '\\') {
					// skip the next escaped character
					i++;
				} else if(c == '\"') {
					currentlyInLiteral = !currentlyInLiteral;
				}
			}
		}
		
		public boolean isInLiteral(int index) {
			return inLiteral[index];
		}
	}
	
	protected static abstract class FormatStage {
		protected Queue<String> lines = new LinkedList<String>();
		
		public boolean hasLineAvailable() {
			return !lines.isEmpty();
		}
		
		public String getLine() {
			return lines.remove();
		}
		
		public abstract void addLine(String line);
	}
	
	protected static class RemoveDuplicateGroupsStage extends FormatStage {
		
		protected class DeletableCharList {
			
			public class DeletableCharListNode {
				public char c;
				public DeletableCharListNode next, prev, pairedNode;
				
				DeletableCharListNode() {
					next = this;
					prev = this;
				}
				
				DeletableCharListNode(char c) {
					this();
					this.c = c;
				}
				
				public DeletableCharListNode nextNonWhitespace() {
					DeletableCharListNode node = this;
					while(Character.isWhitespace(node.c) && node != dummy) {
						node = node.next;
					}
					if(node == dummy) {
						throw new RuntimeException("Node does not have a next non-whitespace character");
					}
					return node;
				}
				
				public DeletableCharListNode prevNonWhitespace() {
					DeletableCharListNode node = this;
					while(Character.isWhitespace(node.c) && node != dummy) {
						node = node.prev;
					}
					if(node == dummy) {
						throw new RuntimeException("Node does not have a prev non-whitespace character");
					}
					return node;
				}
				
				public void delete() {
					next.prev = prev;
					prev.next = next;
				}
				
				public void insertAfter(char c) {
					DeletableCharListNode node = new DeletableCharListNode(c);
					node.next = next;
					node.prev = this;
					next.prev = node;
					this.next = node;
				}
			}
			
			protected int size = 0;
			protected DeletableCharListNode dummy = new DeletableCharListNode();

			public void enqueue(char c) {
				size++;
				lastNode().insertAfter(c);
			}

			public char dequeue() {
				if(size == 0) {
					throw new RuntimeException("Cannot pop an empty stack");
				}
				size--;
				char c = dummy.next.c;
				dummy.next.delete();
				return c;
			}
			
			public DeletableCharListNode lastNode() {
				return dummy.prev;
			}
			
		}
		
		protected static final char[][] groupingChars = {
				{'(', ')'},
				{'{', '}'}
		};
		
		protected static final boolean isOpenGroupingChar(char c) {
			for(char[] arr : groupingChars) {
				if(arr[0] == c) {
					return true;
				}
			}
			return false;
		}
		
		protected static final boolean isCloseGroupingChar(char c) {
			for(char[] arr : groupingChars) {
				if(arr[1] == c) {
					return true;
				}
			}
			return false;
		}
		
		protected static final boolean closeCharMatchesOpenChar(char open, char close) {
			for(char[] arr : groupingChars) {
				if(arr[1] == close) {
					return open == arr[0];
				}
			}
			throw new RuntimeException(close + " is not a grouping character");
		}
		
		protected DeletableCharList chars = new DeletableCharList();
		protected Stack<DeletableCharList.DeletableCharListNode> groupingCharStack = new Stack<DeletableCharList.DeletableCharListNode>();

		@Override
		public void addLine(String line) {
			for(int i = 0; i < line.length(); i++) {
				StringWithLiterals stringWithLiterals = new StringWithLiterals(line);
				char c = line.charAt(i);
				chars.enqueue(c);
				if(stringWithLiterals.isInLiteral(i)) {
					continue;
				}
				if(isOpenGroupingChar(c)) {
					groupingCharStack.push(chars.lastNode());
				} else if(isCloseGroupingChar(c)) {
					DeletableCharList.DeletableCharListNode node = groupingCharStack.pop();
					chars.lastNode().pairedNode = node;
					node.pairedNode = chars.lastNode();
					if(!closeCharMatchesOpenChar(node.c, c)) {
						throw new RuntimeException("Cannot close " + node.c + " with " + c);
					}
					DeletableCharList.DeletableCharListNode innerOpenGroup = node.next.nextNonWhitespace();
					DeletableCharList.DeletableCharListNode innerCloseGroup = chars.lastNode().prev.prevNonWhitespace();
					if(node.c == innerOpenGroup.c && c == innerCloseGroup.c &&
							innerOpenGroup.pairedNode == innerCloseGroup) {
						// We have a pattern like {{ ... }} or (( ... ))
						innerOpenGroup.delete();
						innerCloseGroup.delete();
					}
				}
			}
			chars.enqueue('\n');
			
			if(groupingCharStack.isEmpty()) {
				StringBuilder sb = new StringBuilder();
				while(chars.size > 0) {
					char c = chars.dequeue();
					if(c == '\n') {
						lines.add(sb.toString());
						sb = new StringBuilder();
					} else {
						sb.append(c);
					}
				}
			}
		}
		
	}
	
	protected static class DeleteEmptyLinesStage extends FormatStage {

		@Override
		public void addLine(String line) {
			for(char c : line.toCharArray()) {
				if(!Character.isWhitespace(c)) {
					lines.add(line);
					return;
				}
			}
		}
		
	}
	
	protected static class IndentStage extends FormatStage {
		
		static final String indent = "    ";
		
		protected int indentation;
		
		public IndentStage(int indentation) {
			this.indentation = indentation;
		}

		@Override
		public void addLine(String line) {
			StringBuilder sb = new StringBuilder();
			StringWithLiterals stringWithLiterals = new StringWithLiterals(line);
			int smallestIndentation = indentation;
			for(int i = 0; i < line.length(); i++) {
				char c = line.charAt(i);
				// this logic will need to be changed if strings literals are added
				if(c == '{' && !stringWithLiterals.isInLiteral(i)) {
					indentation++;
				} else if(c == '}' && !stringWithLiterals.isInLiteral(i)) {
					smallestIndentation = Math.min(smallestIndentation, --indentation);
				}
			}
			for(int i = 0; i < smallestIndentation; i++) {
				sb.append(indent);
			}
			lines.add(sb.append(line).toString());
		}
	}
	
	protected static class FormatStages extends FormatStage {
		protected List<FormatStage> formatStages;
		
		public FormatStages(List<FormatStage> formatStages) {
			this.formatStages = new LinkedList<FormatStage>(formatStages);
		}

		@Override
		public void addLine(String line) {
			Queue<String> prevLines = new LinkedList<String>();
			prevLines.add(line);
			for(int i = 0; i < formatStages.size(); i++) {
				FormatStage currentStage = formatStages.get(i);
				while(!prevLines.isEmpty()) {
					currentStage.addLine(prevLines.remove());
				}
				while(currentStage.hasLineAvailable()) {
					prevLines.add(currentStage.getLine());
				}
			}
			while(!prevLines.isEmpty()) {
				lines.add(prevLines.remove());
			}
		}
	}
	
	public static StringBuilder format(StringBuilder sb, int indentation) {
		StringBuilder retSb = new StringBuilder();
		
		List<FormatStage> stagesList = new LinkedList<FormatStage>();
		stagesList.add(new RemoveDuplicateGroupsStage());
		stagesList.add(new DeleteEmptyLinesStage());
		stagesList.add(new IndentStage(indentation));
		FormatStage stages = new FormatStages(stagesList);
		
		for(String line : sb.toString().split("\n")) {
			stages.addLine(line);
			while(stages.hasLineAvailable()) {
				retSb.append(stages.getLine()).append("\n");
			}
		}
		return retSb;
	}
}
