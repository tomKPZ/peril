
public class BinaryOperatorLogicalRightShift extends BinaryOperator {

	public BinaryOperatorLogicalRightShift(Expression e1, Expression e2) {
		super(e1, ">>>", e2);
	}

	@Override
	protected String getTranspiledOpString() {
		throw new RuntimeException("There is not C++ operator for logical right shift on signed integers");
	}
	
	@Override
	public StringBuilder transpile() {
		return new StringBuilder("_logicalRightShift(").append(e1.transpile()).append(", ").append(e2.transpile()).append(")");
	}

	@Override
	protected BinaryOperatorTypeCheckStrategy getTypeCheckStrategy() {
		return new NumericTypeCheckStrategy();
	}

	@Override
	protected BinaryOperatorGetTypeStrategy getGetTypeStrategy() {
		return new ShiftGetTypeStrategy();
	}

}
