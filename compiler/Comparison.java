
public class Comparison extends BinaryOperator {
	
	public Comparison(Expression e1, String op, Expression e2) {
		super(e1, op, e2);
	}

	@Override
	public BaseType getTypeImpl() {
		return new BoolType();
	}

	@Override
	protected String getTranspiledOpString() {
		return op;
	}

	@Override
	protected BinaryOperatorTypeCheckStrategy getTypeCheckStrategy() {
		return new BinaryOperatorTypeCheckStrategy() {

			@Override
			public void binaryTypeCheck(BaseType t1, BaseType t2) {
				switch(op) {
				case ">":
				case "<":
				case ">=":
				case "<=":
					if(!t1.isNumeric() || !t2.isNumeric()) {
						throw new RuntimeException("Cannot use binary comparison operators > < >= <= on types " + t1.stringify() + " and " + t2.stringify());
					}
					break;
				case "==":
				case "!=":
					if(!t1.isEqualityComparableTo(t2)) {
						throw new RuntimeException("Expression " + e1.stringify() + " of type " + t1.stringify() + " is not comparable to " + e2.stringify() + " of type " + t2.stringify());
					}
				}
			}
			
		};
	}

	@Override
	protected BinaryOperatorGetTypeStrategy getGetTypeStrategy() {
		return new BooleanGetTypeStrategy();
	}
}
