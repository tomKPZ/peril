
public class VarLookup extends Expression {
	
	private String name;
	private QualifiedType type;
	private boolean isClassMember;
	
	public VarLookup(String name) {
		this.name = name;
	}
	
	@Override
	public StringBuilder stringify() {
		return new StringBuilder(name);
	}
	
	@Override
	public StringBuilder transpile() {
		StringBuilder sb = new StringBuilder();
		if(name.equals("now")) {
			return sb.append("now");
		} else if(name.equals("this")) {
			return sb.append("_this");
		} else {
			sb.append("_deref(");
			if(isClassMember) {
				sb.append("_this->");
			}
			return sb.append(NameMangler.mangle(name)).append(")");
		}
	}
	
	@Override
	public BaseType getTypeImpl() {
		if(type == null) {
			return Scope.getInstance().getVariableType(name).baseType;
		}
		return type.baseType;
	}
	
	@Override
	public void typeCheck() {
		type = Scope.getInstance().getVariableType(name);
		isClassMember = (Scope.getInstance().getVaraibleClassDeclaration(name) != null) && !name.equals("this");
	}
	
	@Override
	public boolean isExpressionAssignable() {
		return true;
	}
	
}
