
public abstract class ClassMemberDeclaration extends Statement {

	public abstract int nVariables();
	public abstract QualifiedType getVariableType(int varId);
	public abstract String getVariableName(int varId);
	public abstract StringBuilder transpileVariableDeclaration(String className);
	
}
