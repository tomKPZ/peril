
public class IfStatement extends Statement {
	private Expression condition;
	private Statement statement;
	public IfStatement(Expression condition, Statement statement) {
		this.condition = condition;
		this.statement = statement;
	}
	@Override
	public StringBuilder stringify() {
		return new StringBuilder("if (").append(condition.stringify()).append(") ").append(statement.stringify());
	}
	@Override
	public StringBuilder transpile() {
		return new StringBuilder("if (").append(condition.transpile()).append(") {\n").append(statement.transpile()).append("\n}");
	}
	@Override
	public void typeCheck() {
		condition.typeCheck();
		if(!condition.getType().isConditional()) {
			throw new RuntimeException("Condition " + condition.stringify() + " in for loop was of type " + condition.getType().stringify() + ", not bool");
		}
		Scope.getInstance().enterScope();
		statement.typeCheck();
		Scope.getInstance().leaveScope();
	}
}
