
public abstract class Statement implements AstNode {
	
	public boolean isClassDeclaration() {
		return false;
	}
	
	public StringBuilder getForwardDeclaration() {
		throw new RuntimeException(stringify() + " does not need a forward declaration");
	}
	
	public StringBuilder getDeclaration() {
		throw new RuntimeException(stringify() + " does not need a declaration");
	}
	
	public StringBuilder getInitializer() {
		throw new RuntimeException(stringify() + " does not need an initializer");
	}
	
	public int nTranspiledStatements() {
		return 1;
	}
	
}
