
public abstract class UnaryOperator extends Expression {
	
	protected String op;
	protected Expression e;
	
	UnaryOperator(String op, Expression e) {
		this.op = op;
		this.e = e;
	}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder(op).append("(").append(e.stringify()).append(")");
	}
	
	protected abstract String getTranspiledOpString();

	@Override
	public StringBuilder transpile() {
		return new StringBuilder(getTranspiledOpString()).append("(").append(e.transpile()).append(")");
	}
	
	protected abstract void unaryTypeCheck();
	
	@Override
	public void typeCheck() {
		e.typeCheck();
		unaryTypeCheck();
	}

	@Override
	public BaseType getTypeImpl() {
		return e.getType();
	}

}
