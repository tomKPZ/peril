
public class AddableTypeCheckStrategy extends BinaryOperatorTypeCheckStrategy {
	
	@Override
	public void binaryTypeCheck(BaseType t1, BaseType t2) {
		if(!t1.isAddableTo(t2)) {
			throw new RuntimeException("Cannot add " + t1.stringify() + " and " + t2.stringify());
		}
	}
	
}
