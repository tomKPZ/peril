import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

public class Scope {
	private static Scope scope;
	public static Scope getInstance() {
		if(scope == null) {
			synchronized(Scope.class) {
				if(scope == null) {
					scope = new Scope();
				}
			}
		}
		return scope;
	}
	
	protected abstract class ScopeType {
		public BaseType getFunctionReturnType() {
			return null;
		}
		
		public boolean isLoop() {
			return false;
		}
		
		public boolean isClassDeclaration() {
			return false;
		}
		
		public ClassDeclaration getClassDeclaration() {
			return null;
		}
	}
	
	protected class GlobalScope extends ScopeType {
		
	}
	
	protected class BracedScope extends ScopeType {
		
	}
	
	protected class FunctionScope extends ScopeType {
		
		public BaseType returnType;
		
		public FunctionScope(BaseType returnType) {
			this.returnType = returnType;
		}
		
		@Override
		public BaseType getFunctionReturnType() {
			return returnType;
		}
	}
	
	protected class ClassScope extends ScopeType {
		
		public ClassDeclaration classDeclaration;
		
		public ClassScope(ClassDeclaration classDeclaration) {
			this.classDeclaration = classDeclaration;
		}
		
		@Override
		public boolean isClassDeclaration() {
			return true;
		}
		
		@Override
		public ClassDeclaration getClassDeclaration() {
			return classDeclaration;
		}
	}

	protected class LoopScope extends ScopeType {
		@Override
		public boolean isLoop() {
			return true;
		}
	}

	Stack<Map<String, QualifiedType>> variables;
	Stack<Map<String, ClassDeclaration>> userTypes;
	Stack<ScopeType> scopeTypes;
	
	public Scope() {
		variables = new Stack<Map<String, QualifiedType>>();
		userTypes = new Stack<Map<String, ClassDeclaration>>();
		
		scopeTypes = new Stack<ScopeType>();
		
		variables.push(new HashMap<String, QualifiedType>());
		userTypes.push(new HashMap<String, ClassDeclaration>());
		
		scopeTypes.push(new GlobalScope());
		ClassDeclaration versionType = new ClassDeclaration("Version");
		addUserType(versionType);
		addVariable("now", new QualifiedType(new QualifierList(), new VersionType()));
	}
	
	public void addVariable(String name, QualifiedType type) {
		if(variables.peek().containsKey(name)) {
			throw new RuntimeException("Cannot redeclare variable " + name + " in the same scope");
		}
		variables.peek().put(name, type);
	}
	
	public void addUserType(ClassDeclaration cd) {
		if(userTypes.peek().containsKey(cd.getClassName())) {
			throw new RuntimeException("Cannot redeclare class " + cd.getClassName() + " in the same scope");
		}
		userTypes.peek().put(cd.getClassName(), cd);
	}
	
	public QualifiedType getVariableType(String name) {
		Iterator<Map<String, QualifiedType>> it = new ReverseVectorIterator<Map<String, QualifiedType>>(variables);
		while(it.hasNext()) {
			Map<String, QualifiedType> scope = it.next();
			if(scope.containsKey(name)) {
				return scope.get(name);
			}
		}
		throw new RuntimeException("No variable " + name);
	}
	
	public ClassDeclaration getVaraibleClassDeclaration(String name) {
		assert variables.size() == scopeTypes.size();
		Iterator<Map<String, QualifiedType>> varIt = new ReverseVectorIterator<Map<String, QualifiedType>>(variables);
		Iterator<ScopeType> scopeIt = new ReverseVectorIterator<ScopeType>(scopeTypes);
		while(varIt.hasNext() && scopeIt.hasNext()) {
			Map<String, QualifiedType> varScope = varIt.next();
			ScopeType scopeType = scopeIt.next();
			if(varScope.containsKey(name)) {
				if(scopeType.isClassDeclaration()) {
					return scopeType.getClassDeclaration();
				} else {
					return null;
				}
			}
		}
		throw new RuntimeException("No variable " + name);
	}
	
	public ClassDeclaration getClassDeclaration(String name) {
		Iterator<Map<String, ClassDeclaration>> it = new ReverseVectorIterator<Map<String, ClassDeclaration>>(userTypes);
		while(it.hasNext()) {
			Map<String, ClassDeclaration> scope = it.next();
			if(scope.containsKey(name)) {
				return scope.get(name);
			}
		}
		throw new RuntimeException("No class " + name);
	}
	
	public BaseType getEnclosingFunctionReturnType() {
		Iterator<ScopeType> it = new ReverseVectorIterator<ScopeType>(scopeTypes);
		while(it.hasNext()) {
			ScopeType scopeType = it.next();
			BaseType returnType = scopeType.getFunctionReturnType();
			if(returnType != null) {
				return returnType;
			}
		}
		return null;
	}
	
	public ClassDeclaration getEnclosingClassDeclaration() {
		Iterator<ScopeType> it = new ReverseVectorIterator<ScopeType>(scopeTypes);
		while(it.hasNext()) {
			ScopeType scopeType = it.next();
			ClassDeclaration classDeclaration = scopeType.getClassDeclaration();
			if(classDeclaration != null) {
				return classDeclaration;
			}
		}
		return null;
	}
	
	public boolean isInFunction() {
		return getEnclosingFunctionReturnType() != null;
	}
	
	public boolean isInLoop() {
		Iterator<ScopeType> it = new ReverseVectorIterator<ScopeType>(scopeTypes);
		while(it.hasNext()) {
			if(it.next().isLoop()) {
				return true;
			}
		}
		return false;
	}
	
	private void pushScope(ScopeType scopeType) {
		variables.push(new HashMap<String, QualifiedType>());
		userTypes.push(new HashMap<String, ClassDeclaration>());
		scopeTypes.push(scopeType);
	}
	
	public void enterScope() {
		pushScope(new BracedScope());
	}
	
	public void enterClassDeclaration(ClassDeclaration classDeclaration) {
		pushScope(new ClassScope(classDeclaration));
	}
	
	public void enterFunction(BaseType type) {
		pushScope(new FunctionScope(type));
	}
	
	public void enterLoop() {
		pushScope(new LoopScope());
	}
	
	public void leaveScope() {
		variables.pop();
		userTypes.pop();
		scopeTypes.pop();
	}
}
