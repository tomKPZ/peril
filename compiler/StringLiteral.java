
public class StringLiteral extends Expression {
	private String text;
	public StringLiteral(String text) {
		this.text = text;
	}
	@Override
	public StringBuilder stringify() {
		return new StringBuilder(text);
	}
	@Override
	public StringBuilder transpile() {
		return new StringBuilder("std::string(").append(text).append(")");
	}
	@Override
	public void typeCheck() {
		
	}
	@Override
	public BaseType getTypeImpl() {
		return new StringType();
	}
}
