import java.util.ArrayList;
import java.util.List;

public class TupleExpression extends Expression {
	private List<Expression> elements;
	public TupleExpression() {
		elements = new ArrayList<Expression>();
	}

	public void addExpression(Expression e) {
		elements.add(e);
	}
	
	@Override
	public StringBuilder stringify() {
		StringBuilder sb = new StringBuilder("(");
		for(int i = 0; i < elements.size(); i++) {
			sb.append(elements.get(i).stringify());
			if(i != elements.size() - 1) {
				sb.append(",");
			}
		}
		return sb.append(")");
	}

	@Override
	public StringBuilder transpile() {
		StringBuilder sb = new StringBuilder(getType().transpile()).append("(");
		for(int i = 0; i < elements.size(); i++) {
			sb.append(elements.get(i).transpile());
			if(i != elements.size() - 1) {
				sb.append(", ");
			}
		}
		return sb.append(")");
	}

	@Override
	public TupleType getTypeImpl() {
		QualifierList transientQualifier = new QualifierList();
		transientQualifier.addQualifier("transient");
		TupleType t = new TupleType();
		for(Expression e : elements) {
			t.addType(e.getType());
		}
		return t;
	}
	
	@Override
	public boolean isExpressionAssignable() {
		for(int i = 0; i < elements.size(); i++) {
			if(!elements.get(i).isExpressionAssignable()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void typeCheck() {
		for(Expression expression : elements) {
			expression.typeCheck();
		}
	}
	
	@Override
	public int nExpressions() {
		return elements.size();
	}
	
	@Override
	public Expression getSubexpression(int index) {
		if(index >= elements.size() || index < 0) {
			throw new RuntimeException("Cannot get element " + index + " of non-tuple expression " + stringify());
		} else {
			return elements.get(index);
		}
	}
}
