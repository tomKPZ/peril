
public class ByteType extends IntegerType {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("byte");
	}

	@Override
	public int bitWidth() {
		return 8;
	}
	
}
