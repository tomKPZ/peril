
public interface AstNode {
	public StringBuilder stringify();
	public StringBuilder transpile();
	public void typeCheck();
}
