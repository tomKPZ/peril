
public class NullType extends BaseType {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("<NullType>");
	}

	@Override
	public StringBuilder transpile() {
		throw new RuntimeException("NullType is only used internally in the transpiler, and cannot be transpiled");
	}

	@Override
	public void typeCheck() {}

	@Override
	public Expression getDefaultValue() {
		return new NullExpression();
	}
	
	@Override
	public boolean isTypeAssignable() {
		return false;
	}
	
	@Override
	public boolean isEqualityComparableTo(BaseType other) {
		return other.isEqualityComparableTo(this);
	}
	
	@Override
	public boolean isEqualityComparableTo(ObjectType other) {
		return true;
	}
	
	@Override
	public boolean isEqualityComparableTo(FunctionType other) {
		return true;
	}
	
	@Override
	public boolean isEqualityComparableTo(ArrayType other) {
		return true;
	}
	
	@Override
	public boolean isEqualityComparableTo(NullType other) {
		return true;
	}

}
