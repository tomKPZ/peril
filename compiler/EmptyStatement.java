
public class EmptyStatement extends Statement {
	public EmptyStatement() {}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder(";");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder(";");
	}

	@Override
	public void typeCheck() {}
}
