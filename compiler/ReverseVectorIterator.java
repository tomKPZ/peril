import java.util.Iterator;
import java.util.Vector;

public class ReverseVectorIterator<T> implements Iterator<T> {
	
	private Vector<T> stack;
	private int pos;
	
	public ReverseVectorIterator(Vector<T> stack) {
		this.stack = stack;
		pos = stack.size() - 1;
	}
	
	@Override
	public boolean hasNext() {
		return pos >= 0;
	}
	
	@Override
	public T next() {
		T ret = stack.get(pos);
		pos--;
		return ret;
	}
	
	@Override
	public void remove() {
		throw new RuntimeException("Operation not supported");
	}
	
}
