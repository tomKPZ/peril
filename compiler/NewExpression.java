
public class NewExpression extends Expression {
	
	private BaseType type;
	public NewExpression(BaseType type) {
		this.type = type;
	}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("new ").append(type.stringify());
	}

	@Override
	public StringBuilder transpile() {
		return type.transpileNewExpression();
	}

	@Override
	public void typeCheck() {
		type.typeCheck();
	}

	@Override
	public BaseType getTypeImpl() {
		return type;
	}

}
