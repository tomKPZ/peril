
public class BoolType extends BaseType {

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("bool");
	}

	@Override
	public StringBuilder transpile() {
		return new StringBuilder("bool");
	}

	@Override
	public void typeCheck() {}

	@Override
	public Expression getDefaultValue() {
		return new FalseExpression();
	}
	
	@Override
	public boolean isConditional() {
		return true;
	}
	
	@Override
	public boolean isEqualityComparableTo(BaseType other) {
		return other.isEqualityComparableTo(this);
	}

	@Override
	public boolean isEqualityComparableTo(BoolType other) {
		return true;
	}
}
