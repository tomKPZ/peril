import java.util.LinkedList;
import java.util.List;

public class TupleType extends BaseType {
	
	protected List<BaseType> types;
	
	public TupleType() {
		this.types = new LinkedList<BaseType>();
	}
	
	public void addType(BaseType type) {
		types.add(type);
	}
	
	@Override
	public int nSubtypes() {
		return types.size();
	}
	
	@Override
	public BaseType getSubtype(int index) {
		if(index < 0 || index >= types.size()) {
			throw new RuntimeException("Access to element " + index + " to tuple " + stringify() + " was out of bounds");
		}
		return types.get(index);
	}

	@Override
	public StringBuilder stringify() {
		StringBuilder sb = new StringBuilder("(");
		for(int i = 0; i < types.size(); i++) {
			sb.append(types.get(i).stringify());
			if(i != types.size() - 1) {
				sb.append(", ");
			}
		}
		return sb.append(")");
	}

	@Override
	public StringBuilder transpile() {
		StringBuilder sb = new StringBuilder("std::tuple<");
		for(int i = 0; i < types.size(); i++) {
			sb.append(types.get(i).transpile());
			if(i != types.size() - 1) {
				sb.append(", ");
			}
		}
		return sb.append(">");
	}

	@Override
	public void typeCheck() {
		for(BaseType type : types) {
			type.typeCheck();
		}
	}
	
	@Override
	public Expression getDefaultValue() {
		TupleExpression e = new TupleExpression();
		
		for(BaseType type : types) {
			e.addExpression(type.getDefaultValue());
		}
		
		return e;
	}
	
	@Override
	public boolean isEqualityComparableTo(BaseType other) {
		return other.isEqualityComparableTo(this);
	}
	
	@Override
	public boolean isEqualityComparableTo(TupleType other) {
		if(types.size() != other.types.size()) {
			return false;
		} else {
			for(int i = 0; i < types.size(); i++) {
				if(!types.get(i).isEqualityComparableTo(other.types.get(i))) {
					return false;
				}
			}
			return true;
		}
	}
	
}
