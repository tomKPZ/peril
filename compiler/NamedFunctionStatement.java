
public class NamedFunctionStatement extends ClassMemberDeclaration {
	
	private QualifierList qList;
	private String name;
	private StatementList body;
	private NamedTuple arguments;
	private BaseType returnType;
	private QualifiedType type;
	private FunctionType baseType;
	
	public NamedFunctionStatement(QualifierList qList, FunctionType baseType, String name, NamedTuple arguments, BaseType returnType, StatementList body) {
		this.qList = qList;
		this.baseType = baseType;
		this.name = name;
		this.arguments = arguments;
		this.returnType = returnType;
		this.body = body;
		if(new TupleType().isAssignableFrom(returnType)) {
			this.body.add(new ReturnStatement(new TupleExpression()));
		}
	}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder("var ").append(name).append(" = ").append(arguments.stringify()).append(" -> ").append(returnType.stringify()).append(" {\n").append(body.stringify()).append("};");
	}

	protected StringBuilder transpileAux(boolean isClassMember, String className) {
		StringBuilder sb = new StringBuilder();
		if(isClassMember) {
			sb.append("_this->");
		} else {
			sb.append(type.transpile()).append(" *");
		}
		sb.append(NameMangler.mangle(name)).append(" = new ").append(type.transpile()).append("(");
		sb.append(baseType.transpile());
		sb.append("([=] ");
		sb.append(arguments.transpile());
		sb.append(" mutable -> ");
		sb.append(returnType.transpile());
		sb.append(" {\n");
		
		sb.append(baseType.transpile()).append(" ").append(NameMangler.mangle(name)).append(" = [&] ");
		sb.append(arguments.transpile()).append(" -> ").append(returnType.transpile()).append(" {\n");
		
		// make arguments optionally persistent
		for(NamedTuple.Field field : arguments.fields) {
			sb.append(field.getType().transpile()).append(" *").append(field.getName()).append(" = new ").append(field.getType().transpile()).append("(_").append(field.getName()).append(");\n");
		}
		
		sb.append(body.transpile());
		sb.append("};\n");
		sb.append("return ").append(NameMangler.mangle(name)).append("(");
		for(int i = 0; i < arguments.fields.size(); i++) {
			NamedTuple.Field field = arguments.fields.get(i);
			sb.append("_" + field.getName());
			if(i != arguments.fields.size() - 1) {
				sb.append(", ");
			}
		}
		sb.append(");\n");
		return sb.append("}));");
	}
	
	@Override
	public StringBuilder transpile() {
		return transpileAux(false, null);
	}
	
	@Override
	public StringBuilder transpileVariableDeclaration(String className) {
		return transpileAux(true, className);
	}

	@Override
	public void typeCheck() {
		returnType.typeCheck();
		arguments.typeCheck();
		
		FunctionType baseType = new FunctionType(arguments.toBaseType(), returnType);
		if(this.baseType != null) {
			if(!this.baseType.isAssignableFrom(baseType)) {
				throw new RuntimeException("Cannot assign function of type " + baseType.stringify() + " to variable of type " + baseType.stringify());
			}
		} else {
			this.baseType = baseType;
		}
		type = new QualifiedType(qList, this.baseType);
		Scope.getInstance().addVariable(name, type);
		
		Scope.getInstance().enterFunction(returnType);
		
		for(NamedTuple.Field field : arguments.fields) {
			Scope.getInstance().addVariable(field.getName(), field.getType());
		}
		
		body.typeCheck();
		
		Scope.getInstance().leaveScope();
	}

	@Override
	public int nVariables() {
		return 1;
	}

	@Override
	public QualifiedType getVariableType(int varId) {
		if(varId != 0) {
			throw new IllegalArgumentException();
		}
		if(type != null) {
			return type;
		} else {
			if(baseType != null) {
				return new QualifiedType(qList, baseType);
			} else {
				return new QualifiedType(qList, new FunctionType(arguments.toBaseType(), returnType));
			}
		}
	}

	@Override
	public String getVariableName(int varId) {
		if(varId != 0) {
			throw new IllegalArgumentException();
		}
		return name;
	}

}
