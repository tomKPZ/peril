
public class VarInitStatement extends ClassMemberDeclaration {
	
	private NamedTuple varType;
	private Expression exp;
	private NamedTuple.Field[] namedFields;
	
	public VarInitStatement(NamedTuple varType, Expression exp) {
		this.varType = varType;
		this.exp = exp;
		
		namedFields = new NamedTuple.Field[varType.nNamedFields()];
		int curNamed = 0;
		for(int i = 0; i < varType.fields.size(); i++) {
			NamedTuple.Field field = varType.fields.get(i);
			if(field.hasName()) {
				namedFields[curNamed++] = field;
			}
		}
	}
	
	@Override
	public StringBuilder stringify() {
		return new StringBuilder(varType.stringify()).append(" = ").append(exp.stringify()).append(";");
	}
	
	protected StringBuilder transpileAux(boolean isClassMember, String className) {
		StringBuilder sb = new StringBuilder();
		
		if(varType.fields.size() == 1) {
			NamedTuple.Field field = varType.fields.get(0);
			String mangledName = NameMangler.mangle(field.getName());
			if(field.hasName()) {
				if(isClassMember) {
					sb.append("_this->");
				} else {
					sb.append(field.getType().transpile()).append(" *");
				}
				return sb.append(mangledName).append(" = new ").append(field.getType().transpile()).append("(").append(exp.getSubexpression(0).transpile()).append(");");
			} else {
				return sb.append(exp.getSubexpression(0).transpile()).append(";");
			}
		}
		
		int nNamed = 0;
		for(int i = 0; i < varType.fields.size(); i++) {
			if(varType.fields.get(i).hasName()) {
				nNamed++;
			}
		}
		
		// Store the expression into a temporary intermediate variable first,
		// so the expression only gets evaluated once.
		String tempName = NameMangler.getInstance().getTempVarName();
		sb.append(exp.getType().transpile()).append(" ").append(tempName).append("((").append(exp.transpile()).append("));\n");
		
		int curNamed = 0;
		for(int i = 0; i < varType.fields.size(); i++) {
			NamedTuple.Field field = varType.fields.get(i);
			if(field.hasName()) {
				if(isClassMember) {
					sb.append("_this->");
				} else {
					sb.append(field.getType().transpile()).append(" *");
				}
				sb.append(NameMangler.mangle(field.getName())).append(" = new ").append(field.getType().transpile()).append("(std::get<").append(i).append(">(").append(tempName).append("));");
				curNamed++;
			} else {
				return sb.append(exp.getSubexpression(i).transpile()).append(";");
			}
			if(curNamed < nNamed) {
				sb.append("\n");
			}
		}
		return sb;
	}

	@Override
	public StringBuilder transpile() {
		return transpileAux(false, null);
	}
	
	@Override
	public StringBuilder transpileVariableDeclaration(String className) {
		return transpileAux(true, className);
	}

	@Override
	public void typeCheck() {
		varType.typeCheck();
		if(exp == null) {
			exp = varType.toBaseType().getDefaultValue();
		}
		exp.typeCheck();
		BaseType expType = exp.getType();
		if(varType.fields.size() != expType.nSubtypes()) {
			throw new RuntimeException("Cannot assign tuple " + exp.stringify() + " with " + expType.nSubtypes() + " elements to " + varType.stringify() + " with " + varType.fields.size() + " elements");
		}
		for(int i = 0; i < varType.fields.size(); i++) {
			NamedTuple.Field field = varType.fields.get(i);
			if(field.hasName()) {
				if(field.getType().baseType == null) {
					field.getType().baseType = expType.getSubtype(i);
				}
				if(!field.isCompatibleWith(expType.getSubtype(i))) {
					throw new RuntimeException(field.getName() + " of type " + field.getType().stringify() + " is not compatible with expression of type " + expType.getSubtype(i).stringify());
				}
				Scope.getInstance().addVariable(field.getName(), field.getType());
			}
		}
	}
	
	@Override
	public int nTranspiledStatements() {
		if(varType.fields.size() == 1) {
			return 1;
		} else {
			return 1 + varType.nNamedFields();
		}
	}

	@Override
	public int nVariables() {
		return varType.nNamedFields();
	}

	@Override
	public QualifiedType getVariableType(int varId) {
		QualifiedType t = namedFields[varId].getType();
		if(t.baseType == null) {
			return new QualifiedType(t.qualifierList, exp.getType().getSubtype(varId));
		}
		return t;
	}

	@Override
	public String getVariableName(int varId) {
		return namedFields[varId].getName();
	}

}
