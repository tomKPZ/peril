
public class FunctionType extends BaseType {
	
	private BaseType argType;
	private BaseType retType;
	
	public FunctionType(BaseType argType, BaseType retType) {
		this.argType = argType;
		this.retType = retType;
	}

	@Override
	public StringBuilder stringify() {
		return new StringBuilder(argType.stringify()).append(" -> ").append(retType.stringify());
	}

	@Override
	public StringBuilder transpile() {
		StringBuilder sb = new StringBuilder("std::function<").append(retType.transpile()).append("(");
		for(int i = 0; i < argType.nSubtypes(); i++) {
			sb.append(argType.getSubtype(i).transpile());
			if(i != argType.nSubtypes() - 1) {
				sb.append(", ");
			}
		}
		return sb.append(")>");
	}

	@Override
	public void typeCheck() {
		argType.typeCheck();
		retType.typeCheck();
	}

	@Override
	public Expression getDefaultValue() {
		return new NullExpression();
	}
	
	@Override
	public boolean isCallable() {
		return true;
	}
	
	@Override
	public BaseType getArgumentsType() {
		return argType;
	}
	
	@Override
	public BaseType getReturnType() {
		return retType;
	}
	
	@Override
	public boolean isEqualityComparableTo(BaseType other) {
		return other.isEqualityComparableTo(this);
	}
	
	@Override
	public boolean isEqualityComparableTo(NullType other) {
		return true;
	}
	
	@Override
	public boolean isEqualityComparableTo(FunctionType other) {
		return argType.isEqualityComparableTo(other.argType) &&
				retType.isEqualityComparableTo(other.retType);
	}
	
}
