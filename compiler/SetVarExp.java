
public class SetVarExp extends Expression {
	
	Expression left, right;
	
	public SetVarExp(Expression left, Expression right) {
		this.left = left;
		this.right = right;
	}
	
	public StringBuilder stringify() {
		return new StringBuilder("(").append(left.stringify()).append(") = (").append(right.stringify()).append(")");
	}
	
	@Override
	public StringBuilder transpile() {
		StringBuilder sb = new StringBuilder();
		if(left.nExpressions() == 1) {
			sb.append("(").append(left.transpile()).append(") = (").append(right.transpile()).append(")");
		} else {
			sb.append("([&] () {\n");
			
			// Store the expression into a temporary intermediate variable first,
			// so the expression only gets evaluated once.
			final String tempName = NameMangler.getInstance().getTempVarName();
			sb.append(right.getType().transpile()).append(" ").append(tempName).append("((").append(right.transpile()).append("));\n");
			sb.append("return ").append(left.getType().transpile()).append("(");
			for(int i = 0; i < left.nExpressions(); i++) {
				final BaseType rType = right.getType();
				sb.append(new SetVarExp(left.getSubexpression(i), new TupleGetElement(new Expression() {

					@Override
					public StringBuilder stringify() {
						throw new RuntimeException("Operation not supported");
					}

					@Override
					public StringBuilder transpile() {
						return new StringBuilder(tempName);
					}

					@Override
					public void typeCheck() {
						throw new RuntimeException("Operation not supported");
					}

					@Override
					public BaseType getTypeImpl() {
						return rType;
					}
					
				}, i)).transpile());
				if(i != left.nExpressions() - 1) {
					sb.append(", ");
				}
			}
			sb.append(");\n})()");
		}
		return sb;
	}

	@Override
	public BaseType getTypeImpl() {
		return left.getType();
	}
	
	@Override
	public void typeCheck() {
		left.typeCheck();
		if(!left.isExpressionAssignable()) {
			throw new RuntimeException("Cannot assign to " + left.stringify());
		}
		right.typeCheck();
		if(!left.getType().isAssignableFrom(right.getType())) {
			throw new RuntimeException("Expression " + left.stringify() + " of type " + left.getType().stringify() + " cannot be set to expression " + right.stringify() + " of type " + right.getType().stringify());
		}
	}
	
}
