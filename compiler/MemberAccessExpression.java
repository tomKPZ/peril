
public class MemberAccessExpression extends Expression {
	
	private Expression exp;
	private String fieldName;
	private BaseType type;
	
	public MemberAccessExpression(Expression exp, String field) {
		this.exp = exp;
		this.fieldName = field;
	}
	
	@Override
	public StringBuilder stringify() {
		return new StringBuilder("(").append(exp.stringify()).append(").").append(fieldName);
	}
	
	@Override
	public StringBuilder transpile() {
		return new StringBuilder("_deref((").append(exp.transpile()).append(")->").append(NameMangler.mangle(fieldName)).append(")");
	}
	
	@Override
	public BaseType getTypeImpl() {
		return type;
	}
	
	@Override
	public void typeCheck() {
		exp.typeCheck();
		BaseType type = exp.getType();
		this.type = type.getMemberType(fieldName);
	}
	
	@Override
	public boolean isExpressionAssignable() {
		return true;
	}
	
}
