
public class BinaryOperatorLogicalAnd extends BinaryOperator {

	public BinaryOperatorLogicalAnd(Expression e1, Expression e2) {
		super(e1, "and", e2);
	}

	@Override
	protected String getTranspiledOpString() {
		return "&&";
	}

	@Override
	protected BinaryOperatorTypeCheckStrategy getTypeCheckStrategy() {
		return new BooleanTypeCheckStrategy();
	}

	@Override
	protected BinaryOperatorGetTypeStrategy getGetTypeStrategy() {
		return new BooleanGetTypeStrategy();
	}

}
