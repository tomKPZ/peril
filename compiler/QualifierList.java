import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class QualifierList {
	private Set<String> qualifiers;
	
	public QualifierList() {
		qualifiers = new HashSet<String>();
	}
	
	public boolean isTransient() {
		return containsQualifier("transient");
	}
	
	public boolean isPersistent() {
		return !isTransient();
	}
	
	public QualifierList addQualifier(String qualifier) {
		if(containsQualifier(qualifier)) {
			throw new RuntimeException("Duplicate qualifier " + qualifier);
		}
		if(qualifier.equals("transient") && containsQualifier("persistent")) {
			throw new RuntimeException("A variable cannot be both transient and persistent");
		}
		if(qualifier.equals("persistent") && containsQualifier("transient")) {
			throw new RuntimeException("A variable cannot be both transient and persistent");
		}
		qualifiers.add(qualifier);
		return this;
	}
	
	public boolean equals(QualifierList other) {
		return other.isTransient() == isTransient();
	}
	
	public boolean containsQualifier(String qualifier) {
		return qualifiers.contains(qualifier);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Iterator<String> it = qualifiers.iterator();
		for(int i = 0; i < qualifiers.size(); i++) {
			sb.append(it.next());
			if(i != qualifiers.size() - 1) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}
}
