#include "order-maintenance.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <random>
#include <stdexcept>
#include <vector>

void testAssert(bool condition) {
  if (!condition) {
    std::cerr << "assert failed" << std::endl;
    std::abort();
  }
}

void sanityTest(OrderMaintenance* orderMaintenance) {
  OrderMaintenance::Handle h3 =
      orderMaintenance->add(orderMaintenance->getBase());
  OrderMaintenance::Handle h2 =
      orderMaintenance->add(orderMaintenance->getBase());
  OrderMaintenance::Handle h1 =
      orderMaintenance->add(orderMaintenance->getBase());
  testAssert(std::is_lt(orderMaintenance->cmp(h1, h2)));
  testAssert(std::is_lt(orderMaintenance->cmp(h2, h3)));
  testAssert(std::is_lt(orderMaintenance->cmp(h1, h3)));
  testAssert(!std::is_lt(orderMaintenance->cmp(h2, h1)));
  testAssert(!std::is_lt(orderMaintenance->cmp(h3, h2)));
  testAssert(!std::is_lt(orderMaintenance->cmp(h3, h1)));
}

void largeTest(OrderMaintenance* orderMaintenance) {
  static constexpr size_t nItemsBack = 100000;
  static constexpr size_t nItemsFront = 100000;
  static constexpr size_t nItems = nItemsBack + nItemsFront;
  std::vector<OrderMaintenance::Handle> vBack;

  vBack.push_back(orderMaintenance->add(orderMaintenance->getBase()));

  for (size_t i = 1; i < nItemsBack; i++) {
    vBack.push_back(orderMaintenance->add(vBack.back()));
  }

  std::vector<OrderMaintenance::Handle> vFront;
  for (size_t i = 0; i < nItemsFront; i++) {
    vFront.push_back(orderMaintenance->add(orderMaintenance->getBase()));
  }
  std::reverse(vFront.begin(), vFront.end());

  std::vector<OrderMaintenance::Handle> v;
  v.insert(v.end(), vFront.begin(), vFront.end());
  v.insert(v.end(), vBack.begin(), vBack.end());

  static constexpr size_t nChecks = 1000;

  std::default_random_engine rng;

  for (size_t i = 0; i < nChecks; i++) {
    size_t n1 = rng() % nItems;
    size_t n2 = rng() % nItems;
    if (n1 == n2) {
      continue;
    }
    bool ordered = (n1 < n2);
    testAssert(ordered == std::is_lt(orderMaintenance->cmp(v[n1], v[n2])));
  }
}

int main(void) {
  OrderMaintenance* orderMaintenance = new OrderMaintenance();
  sanityTest(orderMaintenance);
  delete orderMaintenance;

  orderMaintenance = new OrderMaintenance();
  largeTest(orderMaintenance);
  delete orderMaintenance;

  return 0;
}
