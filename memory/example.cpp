#include <iostream>

#include "persistent-field.hpp"

struct ListNode {
  PersistentFieldImpl<int> data;
  PersistentFieldImpl<ListNode*> next;
};

void printList(ListNode* list) {
  while (list != nullptr) {
    std::cout << list->data.get(now) << " ";
    list = list->next.get(now);
  }
  std::cout << std::endl;
}

void appendList(ListNode* list, int val) {
  while (list->next.get(now) != nullptr) {
    list = list->next.get(now);
  }
  // list now points to the last node
  ListNode* end = new ListNode();
  now = end->data.set(val, now);
  now = end->next.set(nullptr, now);
  now = list->next.set(end, now);
}

int main(void) {
  ListNode* list = new ListNode();
  now = list->data.set(1, now);
  now = list->next.set(nullptr, now);
  appendList(list, 2);
  Version v1 = now;
  appendList(list, 3);
  appendList(list, 4);
  Version v2 = now;
  appendList(list, 5);
  appendList(list, 6);
  Version v3 = now;
  now = v1;
  printList(list);  // 1 2
  now = v2;
  printList(list);  // 1 2 3 4
  now = v3;
  printList(list);  // 1 2 3 4 5 6
}
