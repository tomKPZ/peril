#ifndef ORDER_MAINTENANCE_H
#define ORDER_MAINTENANCE_H

#include <cassert>
#include <compare>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <numeric>
#include <stdexcept>

#include "macros.hpp"

namespace {

template <typename T>
constexpr void pow2Divide(T log2Numerator,
                          T denominator,
                          T& dividend,
                          T& modulus) {
  static_assert(
      !std::numeric_limits<T>::is_signed && std::numeric_limits<T>::is_integer,
      "pow2Divide only works with unsigned integer types");

  constexpr int nBits = std::numeric_limits<T>::digits;

  assert(log2Numerator <= nBits);

  if (log2Numerator == nBits) {
    T numerator = std::numeric_limits<T>::max();
    dividend = numerator / denominator;
    modulus = 1 + numerator % denominator;
    if (modulus == dividend) {
      dividend++;
      modulus = 0;
    }
  } else {
    T numerator = (T)1 << log2Numerator;
    assert(numerator >= denominator);
    dividend = numerator / denominator;
    modulus = numerator % denominator;
  }
}

}  // namespace

// Implementation of an order maintenance data structure as described
// in "Two Simplified Algorithms for Maintaining Order in a List".
class OrderMaintenance {
 public:
  struct IndirectionNode;
  using Handle = IndirectionNode*;

  OrderMaintenance(void) : first(&base), base(&first) {}

  OrderMaintenance(const OrderMaintenance& other) = delete;
  OrderMaintenance& operator=(const OrderMaintenance& other) = delete;

  Handle getBase(void) {
    assert(base.tag == 0);
    return base.first;
  }

  // Creates an item after oldItem and returns a handle to the new item.
  Handle add(Handle oldItem) {
    IndirectionNode* oldIndirectionNode = (IndirectionNode*)oldItem;
    if (Node::maxNodes != oldIndirectionNode->parent->nIndirectionNodes) {
      // there's room to insert
      size_t newTag = oldIndirectionNode->next == nullptr
                          ? oldIndirectionNode->tag / 2 +
                                std::numeric_limits<size_t>::max() / 2 + 1
                          : std::midpoint(oldIndirectionNode->tag,
                                          oldIndirectionNode->next->tag);

      IndirectionNode* newIndirectionNode = new IndirectionNode(
          oldIndirectionNode->parent, newTag, oldIndirectionNode->next);

      oldIndirectionNode->next = newIndirectionNode;

      oldIndirectionNode->parent->nIndirectionNodes++;

      return newIndirectionNode;
    }

    // we need to split the node

    static constexpr size_t gapSize =
        ((size_t)1 << (std::numeric_limits<size_t>::digits - 1)) /
        Node::maxNodes;

    size_t currentTag = 0;
    IndirectionNode* relabelNode = oldIndirectionNode->parent->first;
    IndirectionNode* prev = nullptr;
    for (int i = 0; i < Node::maxNodes / 2; i++) {
      relabelNode->tag = currentTag;
      currentTag += gapSize;

      prev = relabelNode;
      relabelNode = relabelNode->next;
    }

    prev->next = nullptr;
    prev->parent->nIndirectionNodes = Node::maxNodes / 2;

    Node* newNode =
        add(oldIndirectionNode->parent, relabelNode, Node::maxNodes / 2);

    currentTag = 0;
    for (int i = 0; i < Node::maxNodes / 2; i++) {
      relabelNode->tag = currentTag;
      currentTag += gapSize;

      relabelNode->parent = newNode;

      relabelNode = relabelNode->next;
    }

    return add(oldItem);
  }

  void remove(Handle item) {
    // TODO: implement this
    (void)item;
    std::abort();
  }

  std::strong_ordering cmp(Handle item1, Handle item2) {
    if (LIKELY(item1->parent == item2->parent))
      return item1->tag <=> item2->tag;
    else
      return item1->parent->tag <=> item2->parent->tag;
  }

  void clear(void) {
    Node* next = &base;
    while (next != nullptr) {
      Node* current = next;
      next = next->next;

      // free all IndirectionNodes for the current Node
      IndirectionNode* iNext = current->first;
      while (iNext != nullptr) {
        IndirectionNode* iCurrent = iNext;
        iNext = iNext->next;
        if (iCurrent != &first) {
          delete iCurrent;
        }
      }
      if (current != &base) {
        delete current;
      }
    }
    assert(first.parent == &base);
    assert(first.tag == 0);
    first.next = nullptr;
    assert(base.tag == 0);
    assert(base.prev == nullptr);
    base.next = nullptr;
    assert(base.first == &first);
    base.nIndirectionNodes = 1;
  }

  ~OrderMaintenance(void) { clear(); }

 private:
  struct Node;

  Node* add(Node* oldNode, IndirectionNode* first, size_t nIndirectionNodes) {
    size_t newTag =
        oldNode->next == nullptr
            ? oldNode->tag / 2 + std::numeric_limits<size_t>::max() / 2 + 1
            : std::midpoint(oldNode->tag, oldNode->next->tag);

    Node* newNode =
        new Node(newTag, first, oldNode, oldNode->next, nIndirectionNodes);
    oldNode->next = newNode;
    if (newNode->next != nullptr) {
      newNode->next->prev = newNode;
    }

    if (newNode->tag > oldNode->tag) {
      // there was room inbetween oldItem and oldItem->next for newNode
      return newNode;
    }

    // We need to rebalance. Find the smallest tag range that is
    // non-overflowing.

    Node* tagRangeLoNode = oldNode;
    Node* tagRangeHiNode = newNode;

    size_t tagRangeLo = tagRangeLoNode->tag;
    size_t tagRangeHi = tagRangeHiNode->tag;

    size_t rangeMask = 1;

    static_assert(
        std::numeric_limits<float>::max() > std::numeric_limits<size_t>::max(),
        "your architecture's float cannot represent range(size_t)");

    // oldNode and newNode are currently in a range of size 1
    size_t nNodes = 2;
    size_t logRangeSize = 0;
    float rangeSize = 1.0f;
    float overflowThreshold = 1.0f;

    // density = nNodes/rangeSize
    // while(density > overflowThreshold)
    while (nNodes > overflowThreshold * rangeSize) {
      logRangeSize++;
      rangeSize *= 2.0f;

      bool rangeGrewLeft = tagRangeLo & rangeMask;

      tagRangeLo &= ~rangeMask;
      tagRangeHi |= rangeMask;
      rangeMask <<= 1;

      // Count the additional nodes in this larger range.
      if (rangeGrewLeft) {
        while (tagRangeLoNode->prev != nullptr &&
               tagRangeLoNode->prev->tag >= tagRangeLo) {
          tagRangeLoNode = tagRangeLoNode->prev;
          nNodes++;
        }
      } else {
        while (tagRangeHiNode->next != nullptr &&
               tagRangeHiNode->next->tag <= tagRangeHi) {
          tagRangeHiNode = tagRangeHiNode->next;
          nNodes++;
        }
      }

      static constexpr float inverseT = 1 / T;
      overflowThreshold *= inverseT;
    }

    // Now, we need to uniformly distribute tags between tagRangeLo and
    // tagRangeHi to the nNodes nodes from tagRangeLoNode to tagRangeHiNode.

    size_t gapSize;
    size_t gapSizeRemainder;
    pow2Divide(logRangeSize, nNodes, gapSize, gapSizeRemainder);

    size_t currentPos = tagRangeLo;
    size_t currentPosRemainder = 0;

    Node* currentNode = tagRangeLoNode;
    Node* sentinel = tagRangeHiNode->next;
    size_t nNodesRelabeled = 0;
    while (currentNode != sentinel) {
      currentNode->tag = currentPos;
      nNodesRelabeled++;

      currentPos += gapSize;
      currentPosRemainder += gapSizeRemainder;
      if (currentPosRemainder >= nNodes) {
        currentPos++;
        currentPosRemainder -= nNodes;
      }
      currentNode = currentNode->next;
    }
    assert(nNodesRelabeled == nNodes);

    return newNode;
  }

  void remove(Node* node) {
    node->prev->next = node->next;
    if (node->next != nullptr) {
      node->next->prev = node->prev;
    }
    delete node;
  }

  void splitNode(Node* node) {
    // TODO: implement this
    (void)node;
    std::abort();
  }

  // Overflow threshold constant.
  static constexpr float T = 1.5;
  static_assert(T > 1 && T < 2,
                "overflow threshold constant T must be in (1,2)");

 public:
  // TODO: This should be private.
  struct IndirectionNode {
    size_t tag;
    IndirectionNode* next;
    Node* parent;

    IndirectionNode(Node* parent) : tag(0), next(nullptr), parent(parent) {}

    IndirectionNode(Node* parent, size_t tag, IndirectionNode* next)
        : tag(tag), next(next), parent(parent) {}

    ~IndirectionNode(void) {}
  };

 private:
  struct Node {
    size_t tag;
    Node* prev;
    Node* next;

    IndirectionNode* first;
    uint8_t nIndirectionNodes;

    static constexpr auto maxNodes = std::numeric_limits<size_t>::digits;
    static_assert(std::numeric_limits<decltype(nIndirectionNodes)>::max() >=
                  maxNodes);

    Node(size_t tagUpper,
         IndirectionNode* first,
         Node* prev,
         Node* next,
         size_t nIndirectionNodes)
        : tag(tagUpper),
          prev(prev),
          next(next),
          first(first),
          nIndirectionNodes(nIndirectionNodes) {
#if defined(NDEBUG)
      assert(nIndirectionNodes <= maxNodes);
      size_t nNodes = 0;
      while (first != nullptr) {
        nNodes++;
        first = first->next;
      }
      assert(nNodes == nIndirectionNodes);
#endif
    }

    Node(IndirectionNode* first)
        : tag(0),
          prev(nullptr),
          next(nullptr),
          first(first),
          nIndirectionNodes(1) {}

    ~Node(void) {}
  };

  IndirectionNode first;
  Node base;
};

#endif
