//
//                An implementation of top-down splaying
//                    D. Sleator <sleator@cs.cmu.edu>
//                             March 1992
//
//  "Splay trees", or "self-adjusting search trees" are a simple and
//  efficient data structure for storing an ordered set.  The data
//  structure consists of a binary tree, without parent pointers, and no
//  additional fields.  It allows searching, insertion, deletion,
//  deletemin, deletemax, splitting, joining, and many other operations,
//  all with amortized logarithmic performance.  Since the trees adapt to
//  the sequence of requests, their performance on real access patterns is
//  typically even better.  Splay trees are described in a number of texts
//  and papers [1,2,3,4,5].
//
//  The code here is adapted from simple top-down splay, at the bottom of
//  page 669 of [3].  It can be obtained via anonymous ftp from
//  spade.pc.cs.cmu.edu in directory /usr/sleator/public.
//
//  The chief modification here is that the splay operation works even if the
//  item being splayed is not in the tree, and even if the tree root of the
//  tree is NULL.  So the line:
//
//                              t = splay(i, t);
//
//  causes it to search for item with key i in the tree rooted at t.  If it's
//  there, it is splayed to the root.  If it isn't there, then the node put
//  at the root is the last one before NULL that would have been reached in a
//  normal binary search for i.  (It's a neighbor of i in the tree.)  This
//  allows many other operations to be easily implemented, as shown below.
//
//  [1] "Fundamentals of data structures in C", Horowitz, Sahni,
//       and Anderson-Freed, Computer Science Press, pp 542-547.
//  [2] "Data Structures and Their Algorithms", Lewis and Denenberg,
//       Harper Collins, 1991, pp 243-251.
//  [3] "Self-adjusting Binary Search Trees" Sleator and Tarjan,
//       JACM Volume 32, No 3, July 1985, pp 652-686.
//  [4] "Data Structure and Algorithm Analysis", Mark Weiss,
//       Benjamin Cummins, 1992, pp 119-130.
//  [5] "Data Structures, Algorithms, and Performance", Derick Wood,
//       Addison-Wesley, 1993, pp 367-375.
//

#include <compare>
#include <utility>

template <typename K, typename V>
class SplayTree {
 public:
  SplayTree() = default;
  SplayTree(const SplayTree& other) = delete;
  SplayTree& operator=(const SplayTree& other) = delete;

  void Insert(const K& key, const V& value) {
    if (!root_) {
      root_ = new Node(key, value);
      return;
    }

    Splay(key);
    if (key <=> root_->key() < 0) {
      Node* node = new Node(key, value);
      node->left = root_->left;
      node->right = root_;
      root_->left = nullptr;
      root_ = node;
    } else if (key <=> root_->key() > 0) {
      Node* node = new Node(key, value);
      node->left = root_;
      node->right = root_->right;
      root_->right = nullptr;
      root_ = node;
    } else {
      root_->key_value = {key, value};
    }
  }

  void Erase(const K& key) {
    if (!root_)
      return;

    Splay(key);
    if (key <=> root_->key() != 0)
      return;

    // found it
    Node* old_root = root_;
    if (root_->left) {
      root_ = root_->left;
      Splay(key);
      root_->right = old_root->right;
    } else {
      root_ = root_->right;
    }
    delete old_root;
  }

  const std::pair<K, V>* FindLower(const K& key) {
    Node* pred = Splay(key).first;

    if (root_ && key <=> root_->key() >= 0)
      return &root_->key_value;

    return pred ? &pred->key_value : nullptr;
  }

  const std::pair<K, V>* FindHigher(const K& key) {
    Node* succ = Splay(key).second;

    if (root_ && key <=> root_->key() <= 0)
      return &root_->key_value;

    return succ ? &succ->key_value : nullptr;
  }

 private:
  friend class SplayTreeTest;

  struct Node {
    Node(const K& key, const V& value) : key_value{key, value} {}

    K& key() { return key_value.first; }

    Node* left = nullptr;
    Node* right = nullptr;
    std::pair<K, V> key_value;
  };

  std::pair<Node*, Node*> Splay(const K& key) {
    if (!root_)
      return {nullptr, nullptr};

    Node split({}, {});
    Node* left = &split;
    Node* right = &split;

    while (true) {
      if (key <=> root_->key() < 0) {
        if (!root_->left)
          break;

        if (key <=> root_->left->key() < 0) {
          // rotate right
          Node* rotate_with = root_->left;
          root_->left = rotate_with->right;
          rotate_with->right = root_;
          root_ = rotate_with;
          if (!root_->left)
            break;
        }
        // link right
        right->left = root_;
        right = root_;
        root_ = root_->left;
      } else if (key <=> root_->key() > 0) {
        if (!root_->right)
          break;

        if (key <=> root_->right->key() > 0) {
          // rotate left
          Node* rotate_with = root_->right;
          root_->right = rotate_with->left;
          rotate_with->left = root_;
          root_ = rotate_with;
          if (!root_->right)
            break;
        }
        // link left
        left->right = root_;
        left = root_;
        root_ = root_->right;
      } else {
        break;
      }
    }

    // assemble
    left->right = root_->left;
    right->left = root_->right;
    root_->left = split.right;
    root_->right = split.left;

    return {root_->left ? left : nullptr, root_->right ? right : nullptr};
  }

  Node* root_ = nullptr;
};
