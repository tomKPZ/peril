#ifndef GLOBALS_HPP
#define GLOBALS_HPP

#include <compare>

#include "order-maintenance.hpp"

using Version = OrderMaintenance::Handle;

// This should only be included once per DSO, otherwise the ODR will
// be violated.
OrderMaintenance versions;
Version now = versions.getBase();

#endif
