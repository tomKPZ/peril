#include "splay-tree.hpp"

#include <cassert>
#include <compare>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <set>
#include <string>

void testAssert(bool condition) {
  if (!condition) {
    std::cerr << "assert failed" << std::endl;
    std::abort();
  }
}

using T = uint16_t;

class SplayTreeTest : public SplayTree<T, std::string> {
 public:
  void Test() {
    const int seed = 1234;
    const int items = 5000;

    srand(seed);
    for (int i = 0; i < items; i++) {
      T value = Rand();
      Insert(value);
    }

    Insert(std::numeric_limits<T>::min());
    Insert(std::numeric_limits<T>::max());

    for (int i = 0; i < items; i++) {
      CheckConsistency();
      T value = Rand();
      auto* lower = FindLower(value);
      auto* higher = FindHigher(value);
      auto it = set_.lower_bound(value);
      if (*it == value) {
        testAssert(lower->first == *it);
        testAssert(lower->second == std::to_string(*it));
        testAssert(higher->first == *it);
        testAssert(higher->second == std::to_string(*it));
      } else {
        testAssert(lower->first == *--it);
        testAssert(lower->second == std::to_string(*it));
        testAssert(higher->first == *++it);
        testAssert(higher->second == std::to_string(*it));
      }
    }

    {
      Erase(std::numeric_limits<T>::min());
      auto* lower = FindLower(std::numeric_limits<T>::min());
      auto* higher = FindHigher(std::numeric_limits<T>::min());
      testAssert(!lower && higher);
    }
    {
      Erase(std::numeric_limits<T>::max());
      auto* lower = FindLower(std::numeric_limits<T>::max());
      auto* higher = FindHigher(std::numeric_limits<T>::max());
      testAssert(lower && !higher);
    }

    srand(seed);
    for (int i = 0; i < items; i++) {
      T value = Rand();
      Erase(value);
    }
    CheckConsistency();
  }

 private:
  void Insert(const T& t) {
    CheckConsistency();
    SplayTree<T, std::string>::Insert(t, std::to_string(t));
    set_.insert(t);
  }

  void Erase(const T& t) {
    CheckConsistency();
    SplayTree<T, std::string>::Erase(t);
    set_.erase(t);
  }

  T Rand() {
    T value = rand();
    if (value == std::numeric_limits<T>::min() ||
        value == std::numeric_limits<T>::max()) {
      return Rand();
    }
    return value;
  }

  void CheckConsistency() {
    auto it = set_.begin();
    CheckConsistency(root_, it);
    testAssert(it == set_.end());
  }

  static void CheckConsistency(Node* t, std::set<T>::iterator& it) {
    if (!t)
      return;
    CheckConsistency(t->left, it);
    testAssert(t->key() == *it++);
    CheckConsistency(t->right, it);
  }

  static Node* Max(Node* t) {
    testAssert(t);
    return t->right ? Max(t->right) : t;
  }

  static Node* Min(Node* t) {
    testAssert(t);
    return t->left ? Min(t->left) : t;
  }

  std::set<T> set_;
};

int main() {
  SplayTreeTest test;
  test.Test();

  struct S {
    std::strong_ordering operator<=>(const S&) const {
      return std::strong_ordering::equal;
    }
  };
  SplayTree<S, S> tree;
  tree.Insert(S(), S());
  tree.FindLower(S());
  tree.FindHigher(S());
  tree.Erase(S());

  return 0;
}
