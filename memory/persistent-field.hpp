#ifndef PERSISTENT_FIELD_HPP
#define PERSISTENT_FIELD_HPP

#include <map>

#include "globals.hpp"
#include "splay-tree.hpp"

template <typename T>
class PersistentFieldImpl {
 public:
  PersistentFieldImpl() = default;
  PersistentFieldImpl(const PersistentFieldImpl&) = delete;
  PersistentFieldImpl& operator=(const PersistentFieldImpl&) = delete;

  const T& get(Version version) const {
    const auto* key_value = values.FindLower({version, false});
    assert(key_value && key_value->first.valid());
    return key_value->second;
  }

  Version set(const T& value, Version version) {
    Version end = versions.add(version);
    Version start = versions.add(version);

    const auto* key_value = values.FindLower({version, false});
    bool valid = key_value && key_value->first.valid();
    values.Insert({end, valid}, valid ? key_value->second : T{});
    values.Insert({start, true}, value);

    return start;
  }

 private:
  struct TaggedVersion {
    TaggedVersion() = default;
    TaggedVersion(Version version, bool valid) {
      version_ = reinterpret_cast<uintptr_t>(version);
      assert(!(version_ & 0b1));
      version_ |= valid;
    }
    std::strong_ordering operator<=>(TaggedVersion other) const {
      return versions.cmp(version(), other.version());
    }
    Version version() const {
      return reinterpret_cast<Version>(version_ & ~0b1);
    }
    bool valid() const { return version_ & 0b1; }

   private:
    uintptr_t version_;
  };

  mutable SplayTree<TaggedVersion, T> values;
};

#endif
