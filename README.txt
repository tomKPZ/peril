PERIL: PERsistent Imperative Language

One of the biggest benefits of functional languages is data persistence.
However, most industry standard languages are imperative and have mutable
memory by default.  PERIL combines the safety of persistence with the freedom
and flexibility of an imperative language.

DEMO CODE:

int i = 1; // variables are persistent by default
transient int j = 2; // variables can opt-out of persistence using the transient keyword

// The entire state of memory is stored in a version.  Versions can be stored in
// variables of type 'Version'.  There is a special variable 'Version now' that holds
// the current version, and can be read from to record a version or written into
// to set a version.  These operations run in constant time.

Version v = now; // Version v now holds the point in time where i was 1
// Often, Version variables are transient.

i = 3;
j = 4;

now = v; // now i changes to from 3 to 1, and j remains 4

println i; // 1
println j; // 4

To compile and run the demo code, save it into 'demo.per' and run
    pc.sh path/to/demo.per
This will create an executable 'demo' in the same directory.

Whenever you change 'now' to a different version, you can read/write any
variables in your entire memory, and whenever you visit this version again,
you can be sure all of your non-transient variables will be exactly as you
left them!  You can go on to make changes in memory, record new versions, etc.
