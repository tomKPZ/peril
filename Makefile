CXX=clang++
ANTLR_JAR=./ANTLR/antlr-4.5.1-complete.jar
CXXFLAGS=-Wall -Wextra -std=c++2a -stdlib=libc++
LDFLAGS=

OPT_CFLAGS=-O3 -DNDEBUG -fno-rtti -fno-exceptions -march=native -mtune=native
OPT_LDFLAGS=-ldl

MEM_DIR=./memory
COMP_DIR=./compiler
TEST_DIR=./test

MEM_DEP=$(addprefix $(MEM_DIR)/,globals.hpp order-maintenance.hpp persistent-field.hpp splay-tree.hpp macros.hpp)
MEM_EXE=$(addprefix $(MEM_DIR)/,order-maintenance-test.out splay-tree-test.out example.out)

build?=release
ifeq ($(build),debug)
    CXXFLAGS += -g -O0 -DDEBUG
else ifeq ($(build),profiling)
    CXXFLAGS += -g $(OPT_CFLAGS)
    LDFLAGS += $(OPT_LDFLAGS)
else ifeq ($(build),release)
    CXXFLAGS += $(OPT_CFLAGS)
    LDFLAGS += $(OPT_LDFLAGS)
else
    $(error build must be set to one of {debug,profiling,release})
endif

SKEL_FILE=$(COMP_DIR)/skeleton.cpp

src?=example.per
COMP_BASE_FILE=$(basename $(src))
COMP_PER_FILE=$(COMP_BASE_FILE).per
ifneq ($(src),$(COMP_PER_FILE))
    $(error File extension must be .per)
endif
COMP_CPP_FILE=$(COMP_BASE_FILE).cpp
COMP_EXE_FILE=$(COMP_BASE_FILE)

.PHONY: all
all: $(MEM_EXE) compiler

$(COMP_CPP_FILE): $(COMP_PER_FILE) compiler $(SKEL_FILE)
	java -cp $(COMP_DIR):$(ANTLR_JAR) Compiler $(COMP_PER_FILE) $(SKEL_FILE) > $(COMP_CPP_FILE)

$(COMP_EXE_FILE): $(COMP_CPP_FILE)  $(MEM_DEP)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -I $(MEM_DIR) $(COMP_CPP_FILE) -o $(COMP_EXE_FILE) -fvisibility=hidden -D_LIBCPP_OVERRIDABLE_FUNC_VIS="__attribute__((visibility(\"hidden\")))" -fvisibility-global-new-delete-hidden

.PHONY: compile
compile: $(COMP_EXE_FILE)

%.out: %.o
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -o $@

%.o: %.cpp $(MEM_DEP)
	$(CXX) $(CXXFLAGS) -c $< -o $@

ANTLR_GEN_PREFIX=Peril
ANTLR_GEN_JAVA=$(addprefix $(COMP_DIR)/$(ANTLR_GEN_PREFIX),BaseListener.java Lexer.java Listener.java Parser.java)
ANTLR_GEN=$(ANTLR_GEN_JAVA) $(addprefix $(COMP_DIR)/$(ANTLR_GEN_PREFIX),.tokens Lexer.tokens)
JAVA_DEPS=$(addprefix $(COMP_DIR)/,Statement.java Expression.java ExprStatement.java SetVarExp.java VarInitStatement.java StatementList.java BaseType.java Function.java AstNode.java QualifierList.java FunctionType.java TupleType.java NamedTuple.java QualifiedType.java ClassDeclaration.java WhileLoop.java MemberAccessExpression.java IntegerConstant.java Comparison.java UnaryOperatorLogicalNot.java BinaryOperatorLogicalAnd.java BinaryOperatorLogicalOr.java CodeBlock.java EmptyStatement.java VarLookup.java FunctionCall.java PrintStatement.java TupleExpression.java IfStatement.java Compiler.java ByteType.java ShortType.java IntType.java LongType.java BoolType.java IntegerType.java PrettyFormatter.java ReverseVectorIterator.java ObjectType.java Scope.java NullExpression.java NullType.java NewExpression.java TrueExpression.java FalseExpression.java TupleGetElement.java VersionType.java ReturnStatement.java IfElseStatement.java NameMangler.java ArrayLookup.java ArrayType.java UnaryOperatorMinus.java StringLiteral.java StringType.java NamedFunctionStatement.java BinaryOperator.java BinaryOperatorTypeCheckStrategy.java BinaryOperatorGetTypeStrategy.java NumericTypeCheckStrategy.java IntegerGetTypeStrategy.java BinaryOperatorPlus.java BinaryOperatorMinus.java BinaryOperatorTimes.java BinaryOperatorDivide.java BinaryOperatorMod.java AddableTypeCheckStrategy.java AddableGetTypeStrategy.java BooleanTypeCheckStrategy.java BooleanGetTypeStrategy.java UnaryOperator.java BinaryOperatorBitwiseOr.java BinaryOperatorBitwiseXor.java BinaryOperatorBitwiseAnd.java UnaryOperatorBitwiseNot.java BinaryOperatorLeftShift.java BinaryOperatorLogicalRightShift.java BinaryOperatorArithmeticRightShift.java ShiftGetTypeStrategy.java BreakStatement.java ContinueStatement.java ForLoop.java ClassMemberDeclaration.java)
COMP_BASE_NAME=$(addprefix $(COMP_DIR)/,Compiler)
ANTLR_GRAMMAR=$(COMP_DIR)/$(ANTLR_GEN_PREFIX).g4

.PHONY: compiler
compiler: $(COMP_BASE_NAME).class

$(COMP_BASE_NAME).class: $(ANTLR_GEN) $(COMP_BASE_NAME).java $(JAVA_DEPS)
	javac -cp $(ANTLR_JAR) $(ANTLR_GEN_JAVA) $(COMP_BASE_NAME).java $(JAVA_DEPS)

$(ANTLR_GEN): antlr.gen.intermediate

.INTERMEDIATE: antlr.gen.intermediate
antlr.gen.intermediate: $(ANTLR_GRAMMAR)
	java -jar $(ANTLR_JAR) $(ANTLR_GRAMMAR)

TEST_PER=$(addprefix $(TEST_DIR)/,class-closure.per multi-assignment.per string.per global-var.per recursion.per hash-map.per eval-exp-once.per function-cascade.per list.per primes.per persistent-function.per tuple.per array.per forward-declaration.per mangle.per)
TEST_TXT=$(addsuffix .txt,$(basename $(TEST_PER)))

.PHONY: memtest
memtest: $(MEM_EXE)
	time $(MEM_DIR)/order-maintenance-test.out
	time $(MEM_DIR)/splay-tree-test.out
	@echo "MEMORY TESTS PASSED"

$(TEST_TXT): compiler $(TEST_PER)
	@echo "Running test on $(basename $@).per"
	make compile -j src=$(basename $@).per
	./$(basename $@) > $(basename $@).out
	cmp $(basename $@).out $@

.PHONY: langtest
langtest: $(TEST_TXT)
	@echo "LANGUAGE TESTS PASSED"

.PHONY: test
test: memtest langtest
	@echo "ALL TESTS PASSED"

TEST_EXE=$(basename $(TEST_PER))
TEST_CPP=$(addsuffix .cpp,$(TEST_EXE))
TEST_OUT=$(addsuffix .out,$(TEST_EXE))

.PHONY: clean
clean:
	rm -rf $(MEM_EXE) $(COMP_DIR)/*.class $(ANTLR_GEN) $(TEST_EXE) $(TEST_CPP) $(TEST_OUT)
