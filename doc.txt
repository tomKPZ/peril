---- Integers ----
long - 64 bit signed integer
int - 32 bit signed integer
short - 16 bit signed integer
byte - 8 bit signed integer

class C {
      transient int i; // int32_t i;
      int j; // PersistentField<int32_t> j;
      transient Object p; // Object *p;
      Object q; // PersistentField<Object *> q;
}

---- Lists ----
// Lists are mappings from integers to values.
// Reading from an index that has not been initialized
// gives the default value for that type.
[int] arr = new [int];
// transient [transient int] arr = new [transient int];
arr[12] = 1234;

---- Variables ----
int i = 3;
long j = 3;
var k = 3; // type inferred to be int

---- Tuples ----
(int, long, byte) t = (3, 3, 3);
(int) is the same as int

---- Named Tuples ----
// can initialize multiple variables at the same time
(var i, var j, int k, _, short s) = (2,3,4,5,6);

---- Structures ----
class C {
      int i;
      long j;
      C c;
}

C c = null;
C c = new C; // all attributes are 0 or null

---- Functions ----
(int i, C c) -> (long, int) {
     return (3, 3);
}

// the type of f is inferred to be int -> int
var f = int i -> int {
    return 3;
}

// no function overloading!

---- Output ----
int i = 3;
print i;
C c = new C;
print c; // a shallow output of c

---- Persistence ----
Reason why <pointer, version> won't work (at least on its own):
it will take O(n) updates to change the tail of a list, and is
basically the purely functional style.
Solution: 2 types of pointers?: <pointer> always in current version,
and <pointer, version>
Or: have a designated Version for pointers.

Or: every pointer is just <pointer>, but with a global version ID?
    special variable: Version now;
Small Problem: can't have pointers into different universes: 
can't directly keep  a collection of a data structure throughout time.
Instead, must implicitly maintain a collection of versions.

(Possible) solution: set/restore "now" on function call/return?

Version now;
// now automatically gets set on write.
// It can be read/written.
int i = 5;
Version v1 = now;
i = 6;
Version v2 = now;
now = v1;
print i; // 5
now = v2;
print i; // 6

Should the stack be transient?
    Ans: no
    	 Having a transient stack actually would not change any behavior,
	 but may make debugging easier.  Pointers on the stack are to fat
	 nodes, so transience doesn't matter.  
    Or should Version variables just be transient?
        No. If there's going to be version variables, I want them
        to be storable in specific versions.
    Or should some variables be opt-in transient?
        This seems to be the solution I'm going with now.
Ex: is it possible to get a pointer from version
C to version B using a persistent stack?
    A
   / \
  B   C
Ans: No. With a persistent stack, versions can only set 'now' to
their ancestor versions, because it would be impossible to obtain
a version ID from one of its decendants or siblings.
