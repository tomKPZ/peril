#! /bin/bash

MAKE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SRC_DIR="$( cd "$( dirname "$1" )" && pwd )"
SRC_BASE="$( basename "$1" )"
SRC_FILE="${SRC_DIR}/${SRC_BASE}"

if [ "$#" -ne 1 ]; then
    echo "Usage: pc.sh [Peril file]"
    exit 1
fi

(cd "${MAKE_DIR}" && make compile -j src="${SRC_FILE}")
